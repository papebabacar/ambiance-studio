function styled(type, _, { styles }) {
  function Styled(props) {
    const childProps = { ...props }
    const componentClassName = styles.cls2 || styles.cls1 // always passthrough if the type is a styled component

    delete childProps.as
    const classNames = [childProps.className || '', componentClassName]

    childProps.className = classNames.join(' ')

    const elem = document.createElement(props.as ? props.as : type)

    Object.entries(childProps).map(([prop, val]) => {
      if (prop === 'children') {
        Array.isArray(val) ? val.map((node) => elem.append(node)) : elem.append(val)
      } else {
        elem[prop] = val
      }
    })

    return elem
  }

  return (props) => Styled(props, null)
}

module.exports = styled
module.exports.styled = styled

if (process.env.NODE_ENV !== 'production') {
  module.exports.css = () => {
    throw new Error('`css` template literal evaluated at runtime!')
  }
}
