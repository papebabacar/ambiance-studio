const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
// const WorkboxPlugin = require('workbox-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const WebpackBarPlugin = require('webpackbar')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  mode: process.env.NODE_ENV,
  devtool: 'source-map',
  entry: './src/app.js',
  output: {
    globalObject: 'this'
  },
  module: {
    defaultRules: [
      {
        type: 'javascript/auto',
        resolve: {}
      },
      {
        test: /\.json$/i,
        type: 'json'
      }
    ],
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'surplus-loader'
          },
          'astroturf/loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { modules: true, sourceMap: true, importLoaders: 1 } },
          'postcss-loader'
        ]
      },
      {
        test: /\.wasm$/,
        use: { loader: 'webassembly-loader', options: { export: 'buffer' } }
      },
      {
        test: /\.worker\.js$/,
        use: { loader: 'worker-loader' }
      },
      {
        test: /\.worklet\.js$/,
        use: { loader: 'worklet-loader' }
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['url-loader']
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['url-loader?limit=100000']
      },
      {
        test: /\.(mid|mp3|wav)$/,
        use: ['file-loader']
      }
    ]
  },
  resolve: {
    alias: {
      '@assets': path.resolve(__dirname, 'src/assets'),
      '@components': path.resolve(__dirname, 'src/components'),
      '@constants': path.resolve(__dirname, 'src/constants'),
      '@core': path.resolve(__dirname, 'src/core'),
      '@store': path.resolve(__dirname, 'src/store'),
      '@utils': path.resolve(__dirname, 'src/utils'),
      '@lib': path.resolve(__dirname, 'src/lib'),
      '@reduce-devtools': path.resolve(__dirname, '@reduce-devtools')
    }
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      hash: true,
      template: 'index.html'
    }),
    new MiniCssExtractPlugin(),
    // new WorkboxPlugin.InjectManifest({
    //   swSrc: 'sw.js',
    //   swDest: 'sw.js',
    //   include: [/\.html$/, /\.js$/, /\.svg$/, /\.css$/, /\.png$/, /\.ico$/]
    // }),
    new BundleAnalyzerPlugin(),
    new WebpackBarPlugin({ profile: true })
  ],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        sourceMap: true,
        cache: true,
        parallel: true,
        terserOptions: {
          ecma: 9,
          ouput: {
            comments: false
          }
        }
      }),
      new CssMinimizerPlugin({
        parallel: true,
        sourceMap: true
      })
    ]
  }
}
