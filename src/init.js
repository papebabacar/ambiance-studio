import S from 's-js'
import { state } from '@store'
import PianorollGridWorklet from '@core/graphics/pianoroll/grid.worklet'
import ParametersGridWorklet from '@core/graphics/pianoroll/parameters.grid.worklet'
import PlaylistGridWorklet from '@core/graphics/playlist/grid.worklet'

CSS.registerProperty({
  name: '--denominator',
  syntax: '<number>',
  initialValue: S.sample(state.denominator),
  inherits: true
})

CSS.registerProperty({
  name: '--numerator',
  syntax: '<number>',
  initialValue: S.sample(state.numerator),
  inherits: true
})

CSS.registerProperty({
  name: '--pianoroll-grid-x',
  syntax: '<number>',
  initialValue: S.sample(state.pianoroll).gridX,
  inherits: true
})

CSS.registerProperty({
  name: '--pianoroll-grid-y',
  syntax: '<number>',
  initialValue: S.sample(state.pianoroll).gridY,
  inherits: true
})

CSS.registerProperty({
  name: '--pianoroll-snap',
  syntax: '<number>',
  initialValue: S.sample(state.pianoroll).snap,
  inherits: true
})

CSS.registerProperty({
  name: '--pianoroll-bars',
  syntax: '<number>',
  initialValue: S.sample(state.pianoroll).grids,
  inherits: true
})

CSS.registerProperty({
  name: '--playlist-grid-x',
  syntax: '<number>',
  initialValue: S.sample(state.playlist).gridX,
  inherits: true
})

CSS.registerProperty({
  name: '--playlist-grid-y',
  syntax: '<number>',
  initialValue: S.sample(state.playlist).gridY,
  inherits: true
})

CSS.registerProperty({
  name: '--playlist-bars',
  syntax: '<number>',
  initialValue: S.sample(state.playlist).grids,
  inherits: true
})

CSS.registerProperty({
  name: '--playlist-snap',
  syntax: '<number>',
  initialValue: S.sample(state.playlist).snap,
  inherits: true
})

CSS.registerProperty({
  name: '--playlist-tracks',
  syntax: '<number>',
  initialValue: S.sample(state.playlist).tracks,
  inherits: true
})

CSS.paintWorklet.addModule(PianorollGridWorklet)

CSS.paintWorklet.addModule(ParametersGridWorklet)

CSS.paintWorklet.addModule(PlaylistGridWorklet)
