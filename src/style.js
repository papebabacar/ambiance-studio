import { css } from 'astroturf'

export default css`
  .app {
    height: 100vh;
    display: grid;
    grid-gap: 0.125rem;
    grid-template-columns: repeat(23, 1fr);
    grid-template-rows: repeat(16, 1fr);
  }
`
