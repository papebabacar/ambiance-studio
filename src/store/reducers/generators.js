import { v1 as uuid } from 'uuid'
import { History } from '@store/history'
import { dispatch } from '@store'
import { GENERATOR_ADD, PATTERN_ADD, GENERATOR_DELETE, GENERATOR_SELECT, GENERATOR_MUTE } from '@store/actions'

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case GENERATOR_ADD: {
      const { id = uuid(), name, type, mute = false } = payload
      state.generators({ ...state.generators(), [id]: { ...state.generators()[id], name, type, mute } })
      state.selectedGenerator(id)
      setImmediate(() => dispatch(PATTERN_ADD, { id, name, generator: id, notes: {} }))
      return
    }
    case GENERATOR_DELETE: {
      const { id } = payload
      const { [id]: deleted, ...generators } = state.generators() // eslint-disable-line no-unused-vars
      state.generators(generators)
      return
    }

    case GENERATOR_SELECT: {
      const { id } = payload
      state.selectedGenerator(id)
      if (id !== state.selectedPattern()) {
        state.selectedPattern(id)
      }
      return
    }
    case GENERATOR_MUTE: {
      History.remember(state)
      const { id, mute } = payload
      state.generators({ ...state.generators(), [id]: { ...state.generators()[id], mute } })
      return
    }
  }
}
