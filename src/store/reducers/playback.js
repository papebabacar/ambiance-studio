import S from 's-js'
import { History } from '@store/history'
import {
  PLAY,
  PAUSE,
  STOP,
  RECORD,
  SEEK,
  SET_TEMPO,
  SONG_LOOP,
  SONG_MODE,
  SET_NUMERATOR,
  SET_DENOMINATOR,
  PIANOROLL_VIEW,
  PLAYLIST_VIEW
} from '@store/actions'
import { Scheduler } from '@core/scheduler'
import { getPlaylistSnapWidth } from '@utils/midi'
import { roundToNearest } from '@utils'

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case PLAY: {
      Scheduler.play()
      state.playing(true)
      return
    }
    case PAUSE: {
      Scheduler.pause()
      state.playing(false)
      return
    }
    case STOP: {
      Scheduler.stop()
      state.playing(false)
      return
    }
    case RECORD: {
      const { recording } = payload
      state.recording(recording)
      return
    }
    case SEEK: {
      const { cursorX } = payload
      Scheduler.seek(cursorX)
      S.sample(state.view) === PLAYLIST_VIEW && state.songMode(true)
      return
    }
    case SONG_LOOP: {
      const { start, end } = payload
      const store = S.sample(state.view) === PIANOROLL_VIEW ? state.pianoroll : state.playlist
      store({ ...store(), start, end })
      return
    }
    case SONG_MODE: {
      const { songMode } = payload
      state.songMode(songMode)
      return
    }
    case SET_TEMPO: {
      History.remember(state)
      const { microsecondsPQN } = payload
      state.microsecondsPQN(microsecondsPQN)
      return
    }
    case SET_NUMERATOR: {
      const { numerator } = payload
      const prevPlaylistSnap = getPlaylistSnapWidth(state.playlist().snapType)
      const playlistGridX = roundToNearest(state.playlist().gridX, numerator)
      state.playlist().gridX = playlistGridX
      document.body.attributeStyleMap.set('--playlist-grid-x', playlistGridX)
      const playlistSnap = getPlaylistSnapWidth(state.playlist().snapType)
      const factor = (playlistSnap * state.numerator()) / (prevPlaylistSnap * numerator)
      state.songMode() && state.cursorX(state.cursorX() * factor)
      state.playlist().currentWidth = state.playlist().currentWidth * factor
      state.playlist().start = state.playlist().start * factor
      state.playlist().end = state.playlist().end * factor
      state.playlist().length = state.playlist().length * factor
      state.clips({
        ...state.clips(),
        ...Object.entries(state.clips()).reduce((clips, [id, { x, width, ...rest }]) => {
          state.dirty[id] = true
          return {
            ...clips,
            [id]: {
              ...rest,
              x: x * factor,
              width: width * factor
            }
          }
        }, {})
      })
      state.numerator(numerator)
      return
    }
    case SET_DENOMINATOR: {
      const { denominator } = payload
      const factor = denominator / state.denominator()
      state.playlist().currentWidth = state.playlist().currentWidth * factor
      state.playlist().start = state.playlist().start * factor
      state.playlist().end = state.playlist().end * factor
      state.playlist().length = state.playlist().length * factor
      state.microsecondsPQN(state.microsecondsPQN() * factor)
      state.clips({
        ...state.clips(),
        ...Object.entries(state.clips()).reduce((clips, [id, { x, width, ...rest }]) => {
          state.dirty[id] = true
          return {
            ...clips,
            [id]: {
              ...rest,
              x: x * factor,
              width: width * factor
            }
          }
        }, {})
      })
      state.denominator(denominator)
      return
    }
  }
}
