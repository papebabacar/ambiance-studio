import { NOTE_RECORD_ON, NOTE_RECORD_OFF } from '@store/actions'
import { Scheduler } from '@core/scheduler'

let recorded

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case NOTE_RECORD_ON: {
      !recorded && History.remember(state)
      recorded = true
      const { id, noteNumber } = payload
      History.markAsAdded(id)
      state.patterns()[state.selectedPattern()].notes[id] = true
      state.notes({
        ...state.notes(),
        [id]: {
          x: Scheduler.cursorX,
          y: noteNumber * state.pianoroll().gridY,
          offset: 0,
          width: 0,
          mute: false,
          pattern: state.selectedPattern()
        }
      })
      return
    }
    case NOTE_RECORD_OFF: {
      const { id } = payload
      state.dirty[id] = true
      History.markAsDirty(id)
      state.notes({
        ...state.notes(),
        [id]: {
          ...state.notes()[id],
          width: Scheduler.cursorX - state.notes()[id].x
        }
      })
      return
    }
  }
}
