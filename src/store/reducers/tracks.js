import { TRACK_MUTE, TRACK_UNMUTE } from '@store/actions'
import { History } from '@store/history'

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case TRACK_MUTE: {
      History.remember(state)
      const { trackIndex } = payload
      state.mutedTracks({ ...state.mutedTracks(), [trackIndex]: true })
      return
    }
    case TRACK_UNMUTE: {
      History.remember(state)
      const { trackIndex } = payload
      const { [trackIndex]: _, ...rest } = state.mutedTracks() // eslint-disable-line no-unused-vars
      state.mutedTracks(rest)
      return
    }
  }
}
