import { NOTE_ON, NOTE_OFF, SILENCE } from '@store/actions'

let noteOnTimeouts = Array(10 * 12).fill()

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case NOTE_ON: {
      const { noteNumber, time = 0, generator = state.selectedGenerator(), track } = payload
      noteOnTimeouts[noteNumber] = setTimeout(() => {
        requestAnimationFrame(() => {
          state.playingNotes({
            ...state.playingNotes(),
            [noteNumber]: {
              ...state.playingNotes()[noteNumber],
              on: time,
              off: null,
              hit: true,
              generator,
              track
            }
          })
        })
        setTimeout(
          () =>
            state.playingNotes({
              ...state.playingNotes(),
              [noteNumber]: {
                ...state.playingNotes()[noteNumber],
                hit: false
              }
            }),
          133.3333333333333
        )
      }, time * 1000)
      return
    }
    case NOTE_OFF: {
      const { noteNumber, time = 0 } = payload
      setTimeout(() => {
        requestAnimationFrame(() => {
          state.playingNotes({
            ...state.playingNotes(),
            [noteNumber]: {
              ...state.playingNotes()[noteNumber],
              on: null,
              off: time
            }
          })
        })
      }, time * 1000)
      return
    }
    case SILENCE: {
      let playingNotes
      Object.entries(state.playingNotes()).map(([noteNumber, playingNote]) => {
        clearTimeout(noteOnTimeouts[noteNumber])
        playingNotes = { ...playingNotes, [noteNumber]: { ...playingNote, on: null, off: 0 } }
      })
      state.scheduledNotes(Array(10 * 12).fill())
      setTimeout(
        () =>
          requestAnimationFrame(() =>
            state.playingNotes({
              ...playingNotes
            })
          ),
        133.33333333333333
      )

      return
    }
  }
}
