import { PIANOROLL_VIEW, PLAYLIST_VIEW } from '@store/actions'

export default (state, action) => {
  const { type } = action
  switch (type) {
    case PIANOROLL_VIEW: {
      state.view(PIANOROLL_VIEW)
      return
    }
    case PLAYLIST_VIEW: {
      state.view(PLAYLIST_VIEW)
      return
    }
  }
}
