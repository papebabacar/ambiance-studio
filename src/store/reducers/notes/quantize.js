import S from 's-js'
import { History } from '@store/history'
import { TOOL_QUANTIZE } from '@store/actions'
import { roundToNearest } from '@utils'
import { getPianorollSnapWidth } from '@utils/midi'

export default (state, action) => {
  const { type, payload } = action

  switch (type) {
    case TOOL_QUANTIZE: {
      History.remember(state)
      const { ids } = payload
      let diff
      const { snap } = S.sample(state.pianoroll)
      const minSnap = Math.max(snap, getPianorollSnapWidth('1/64BEAT'))
      const notes = ids.length ? ids : Object.keys(S.sample(state.patterns)[S.sample(state.selectedPattern)].notes)
      const newNotes = notes.reduce((allNotes, id) => {
        const { x, width } = state.notes()[id]
        if ((x / minSnap) % 1 !== 0 || (width / minSnap) % 1 !== 0) {
          diff = true
          state.dirty[id] = true
          History.markAsDirty(id)
          return {
            ...allNotes,
            [id]: {
              ...state.notes()[id],
              x: roundToNearest(x, minSnap),
              width: Math.max(minSnap, roundToNearest(width, minSnap))
            }
          }
        } else return allNotes
      }, {})
      if (!diff) {
        History.forget()
        return
      }
      state.notes({
        ...state.notes(),
        ...newNotes
      })
      return
    }
  }
}
