import S from 's-js'
import { History } from '@store/history'
import { NOTE_SELECT, NOTE_UNSELECT, SELECTION_MOVE, SELECTION_RESIZE, PIANOROLL_VIEW } from '@store/actions'

export default (state, action) => {
  const notes = S.sample(state.view) === PIANOROLL_VIEW ? state.notes : state.clips
  const { type, payload } = action
  switch (type) {
    case NOTE_SELECT: {
      const { id } = payload
      state.dirty[id] = true
      History.markAsDirty(id)
      notes({
        ...notes(),
        [id]: { ...notes()[id], selected: true }
      })
      return
    }
    case NOTE_UNSELECT: {
      const { id } = payload
      state.dirty[id] = true
      History.markAsDirty(id)
      notes({
        ...notes(),
        [id]: { ...notes()[id], selected: false }
      })
      return
    }
    case SELECTION_MOVE: {
      const { ids, dx, dy } = payload
      const newNotes = ids.reduce((allNotes, id) => {
        state.dirty[id] = true
        History.markAsDirty(id)
        return {
          ...allNotes,
          [id]: { ...notes()[id], x: notes()[id].x + dx, y: notes()[id].y + dy }
        }
      }, {})
      notes({
        ...notes(),
        ...newNotes
      })
      return
    }
    case SELECTION_RESIZE: {
      const { ids, dw, offset = 0 } = payload
      const newNotes = ids.reduce((allNotes, id) => {
        state.dirty[id] = true
        History.markAsDirty(id)
        return {
          ...allNotes,
          [id]: {
            ...notes()[id],
            x: notes()[id].x + offset,
            offset: notes()[id].offset + offset,
            width: notes()[id].width + dw
          }
        }
      }, {})
      notes({
        ...notes(),
        ...newNotes
      })
      return
    }
  }
}
