import { CONTROL_SET, CONTROL_VIEW } from '@store/actions'

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case CONTROL_SET: {
      const notes = state.notes
      const { ids, ...controls } = payload
      ids.map((id) => {
        state.dirty[id] = true
        notes({ ...notes(), [id]: { ...notes()[id], controls: { ...notes()[id]['controls'], ...controls } } })
      })
      return
    }
    case CONTROL_VIEW: {
      const { view } = payload
      state.controlView(view)
      return
    }
  }
}
