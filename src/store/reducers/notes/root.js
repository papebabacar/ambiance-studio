import { v1 as uuid } from 'uuid'
import S from 's-js'
import { History } from '@store/history'
import {
  NOTE_ADD,
  NOTE_MOVE,
  NOTE_RESIZE,
  NOTE_DEFAULT,
  NOTE_DELETE,
  NOTE_MUTE,
  PIANOROLL_VIEW,
  PLAYLIST_VIEW
} from '@store/actions'

export default (state, action) => {
  const notes = S.sample(state.view) === PIANOROLL_VIEW ? state.notes : state.clips
  const { type, payload } = action

  switch (type) {
    case NOTE_ADD: {
      const {
        x,
        y,
        offset = S.sample(state.view) === PIANOROLL_VIEW
          ? state.pianoroll().currentOffsetTime
          : state.playlist().currentOffsetTime,
        width = S.sample(state.view) === PIANOROLL_VIEW
          ? state.pianoroll().currentWidth
          : state.playlist().currentWidth,
        mute = false,
        controls = {},
        points
      } = payload
      if (points) {
        const newNotes = points.reduce((allNotes, { x, y, width }) => {
          const id = uuid()
          History.markAsAdded(id)
          S.sample(state.view) === PIANOROLL_VIEW && (state.patterns()[state.selectedPattern()].notes[id] = true)
          return {
            ...allNotes,
            [id]: {
              x,
              y,
              offset,
              width,
              mute,
              controls,
              pattern: S.sample(state.view) === PLAYLIST_VIEW ? state.selectedPattern() : undefined
            }
          }
        }, {})
        notes({
          ...notes(),
          ...newNotes
        })
      } else {
        const id = uuid()
        History.markAsAdded(id)
        S.sample(state.view) === PIANOROLL_VIEW && (state.patterns()[state.selectedPattern()].notes[id] = true)
        notes({
          ...notes(),
          [id]: {
            x,
            y,
            offset,
            width,
            mute,
            controls,
            pattern: S.sample(state.view) === PLAYLIST_VIEW ? state.selectedPattern() : undefined
          }
        })
      }
      return
    }
    case NOTE_MOVE: {
      const { id, x = notes()[id].x, y = notes()[id].y } = payload
      state.dirty[id] = true
      History.markAsDirty(id)
      notes({ ...notes(), [id]: { ...notes()[id], x, y } })
      return
    }
    case NOTE_DELETE: {
      const { id, ids } = payload
      if (ids) {
        let rests = notes()
        ids.map((id) => {
          History.markAsDeleted(id)
          const { [id]: _, ...rest } = rests // eslint-disable-line no-unused-vars
          rests = { ...rest }
        })
        S.sample(state.view) === PIANOROLL_VIEW &&
          (state.patterns()[state.selectedPattern()].notes = Object.keys(rests).reduce(
            (notes, id) => ({ ...notes, [id]: true }),
            {}
          ))
        notes(rests)
      } else {
        History.markAsDeleted(id)
        S.sample(state.view) === PIANOROLL_VIEW && delete state.patterns()[state.selectedPattern()].notes[id]
        const { [id]: _, ...rest } = notes() // eslint-disable-line no-unused-vars
        notes(rest)
      }
      return
    }
    case NOTE_MUTE: {
      const { id, ids, mute } = payload
      if (ids) {
        const newNotes = ids.reduce((allNotes, id) => {
          state.dirty[id] = true
          History.markAsDirty(id)
          return {
            ...allNotes,
            [id]: { ...notes()[id], mute }
          }
        }, {})
        notes({
          ...notes(),
          ...newNotes
        })
      } else {
        if (mute !== notes()[id].mute) {
          state.dirty[id] = true
          History.markAsDirty(id)
          notes({ ...notes(), [id]: { ...notes()[id], mute } })
        }
      }
      return
    }
    case NOTE_RESIZE: {
      const { id, width, offset = 0 } = payload
      state.dirty[id] = true
      History.markAsDirty(id)
      notes({
        ...notes(),
        [id]: { ...notes()[id], x: notes()[id].x + offset, offset: notes()[id].offset + offset, width }
      })
      return
    }

    case NOTE_DEFAULT: {
      const { currentWidth, currentOffsetTime } = payload
      const store = S.sample(state.view) === PIANOROLL_VIEW ? state.pianoroll : state.playlist
      store().currentWidth = currentWidth
      store().currentOffsetTime = currentOffsetTime
      return
    }
  }
}
