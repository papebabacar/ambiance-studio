import S from 's-js'
import { v1 as uuid } from 'uuid'
import { History } from '@store/history'
import { TOOL_CHOP } from '@store/actions'
import { getPianorollSnapWidth } from '@utils/midi'

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case TOOL_CHOP: {
      const { ids } = payload
      History.remember(state)
      let diff
      const { snap } = S.sample(state.pianoroll)
      const minSnap = Math.max(snap, getPianorollSnapWidth('1/64BEAT'))
      const notes = ids.length ? ids : Object.keys(S.sample(state.patterns)[S.sample(state.selectedPattern)].notes)
      const newNotes = notes.reduce((allNotes, id) => {
        const { width } = state.notes()[id]
        const { x } = state.notes()[id]
        if (minSnap < width) {
          diff = true
          state.dirty[id] = true
          History.markAsDirty(id)
          Array(Math.ceil(width / minSnap) - 1)
            .fill()
            .map((_, index) => {
              const newId = uuid()
              History.markAsAdded(newId)
              const snapX = x + (index + 1) * minSnap
              state.patterns()[state.selectedPattern()].notes[newId] = true
              allNotes[newId] = {
                ...state.notes()[id],
                x: snapX,
                width: Math.min(minSnap, x + width - snapX)
              }
            })
          return {
            ...allNotes,
            [id]: {
              ...state.notes()[id],
              width: minSnap
            }
          }
        } else return allNotes
      }, {})
      if (!diff) {
        History.forget()
        return
      }
      state.notes({
        ...state.notes(),
        ...newNotes
      })
      return
    }
  }
}
