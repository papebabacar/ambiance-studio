import root from './root'
import selection from './selection'
import quantize from './quantize'
import chop from './chop'
import controls from './controls'

const reducers = [root, selection, quantize, chop, controls]

export default function rootReducer(state, action) {
  reducers.map((reduce) => reduce(state, action))
}
