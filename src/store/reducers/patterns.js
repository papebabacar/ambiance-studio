import { v1 as uuid } from 'uuid'
import { PATTERN_ADD, PATTERN_DELETE, PATTERN_SELECT } from '@store/actions'

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case PATTERN_ADD: {
      const { id = uuid(), name, generator, notes = {} } = payload
      state.patterns({ ...state.patterns(), [id]: { ...state.patterns()[id], name, generator, notes } })
      state.selectedPattern(id)
      return
    }
    case PATTERN_DELETE: {
      const { id } = payload
      const { [id]: deleted, ...patterns } = state.patterns() // eslint-disable-line no-unused-vars
      state.patterns(patterns)
      return
    }
    case PATTERN_SELECT: {
      const { id } = payload
      if (id !== state.selectedPattern()) {
        state.selectedPattern(id)
      }
      return
    }
  }
}
