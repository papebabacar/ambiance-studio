import {
  SET_PIANOROLL_SNAP,
  SET_PLAYLIST_SNAP,
  SET_PIANOROLL_GRID_X,
  SET_PLAYLIST_GRID_X,
  SET_PIANOROLL_GRID_Y,
  SET_PLAYLIST_GRID_Y
} from '@store/actions'
import { roundToNearest } from '@utils'
import { getPianorollSnapWidth, getPlaylistSnapWidth } from '@utils/midi'

export default (state, action) => {
  const { type, payload } = action
  switch (type) {
    case SET_PIANOROLL_SNAP: {
      const { snap } = payload
      state.pianoroll().snap = getPianorollSnapWidth(snap)
      state.pianoroll().snapType = snap
      document.body.attributeStyleMap.set('--pianoroll-snap', state.pianoroll().snap)
      return
    }
    case SET_PLAYLIST_SNAP: {
      console.log('setting it')
      const { snap } = payload
      state.playlist().snap = getPlaylistSnapWidth(snap)
      state.playlist().snapType = snap
      document.body.attributeStyleMap.set('--playlist-snap', state.playlist().snap)
      return
    }
    case SET_PIANOROLL_GRID_X: {
      const { grid } = payload
      const prevGridX = state.pianoroll().gridX
      const gridX = roundToNearest(grid, 3)
      const factor = gridX / prevGridX
      state.pianoroll().gridX = gridX
      state.pianoroll().currentWidth = state.pianoroll().currentWidth * factor
      state.pianoroll().start = state.pianoroll().start * factor
      state.pianoroll().end = state.pianoroll().end * factor
      state.pianoroll().length = state.pianoroll().length * factor
      !state.songMode() && state.cursorX(state.cursorX() * factor)
      state.notes({
        ...state.notes(),
        ...Object.entries(state.notes()).reduce((notes, [id, { x, width, ...rest }]) => {
          state.dirty[id] = true
          return {
            ...notes,
            [id]: { ...rest, x: x * factor, width: width * factor }
          }
        }, {})
      })
      state.pianoroll().snap = getPianorollSnapWidth(state.pianoroll().snapType)
      requestAnimationFrame(() => {
        document.body.attributeStyleMap.set('--pianoroll-snap', state.pianoroll().snap)
        document.body.attributeStyleMap.set('--pianoroll-grid-x', gridX)
      })
      return
    }
    case SET_PLAYLIST_GRID_X: {
      const { grid } = payload
      const prevGridX = state.playlist().gridX
      const gridX = roundToNearest(grid, state.numerator())
      const factor = gridX / prevGridX
      state.songMode() && state.cursorX(state.cursorX() * factor)
      state.playlist().gridX = gridX
      state.playlist().currentWidth = state.playlist().currentWidth * factor
      state.playlist().start = state.playlist().start * factor
      state.playlist().end = state.playlist().end * factor
      state.playlist().length = state.playlist().length * factor
      state.clips({
        ...state.clips(),
        ...Object.entries(state.clips()).reduce((clips, [id, { x, width, ...rest }]) => {
          state.dirty[id] = true
          return {
            ...clips,
            [id]: { ...rest, x: x * factor, width: width * factor }
          }
        }, {})
      })
      state.playlist().snap = getPlaylistSnapWidth(state.playlist().snapType)
      requestAnimationFrame(() => {
        document.body.attributeStyleMap.set('--playlist-snap', state.playlist().snap)
        document.body.attributeStyleMap.set('--playlist-grid-x', state.playlist().gridX)
      })
      return
    }
    case SET_PIANOROLL_GRID_Y: {
      const { grid } = payload
      const prevGridY = state.pianoroll().gridY
      state.pianoroll().gridY = grid
      state.notes({
        ...state.notes(),
        ...Object.entries(state.notes()).reduce((notes, [id, { y, ...rest }]) => {
          state.dirty[id] = true
          return {
            ...notes,
            [id]: { ...rest, y: (y * grid) / prevGridY }
          }
        }, {})
      })
      requestAnimationFrame(() => {
        document.body.attributeStyleMap.set('--pianoroll-grid-y', grid)
      })
      return
    }
    case SET_PLAYLIST_GRID_Y: {
      const { grid } = payload
      const prevGridY = state.playlist().gridY
      state.playlist().gridY = grid
      state.clips({
        ...state.clips(),
        ...Object.entries(state.clips()).reduce((clips, [id, { y, ...rest }]) => {
          state.dirty[id] = true
          return {
            ...clips,
            [id]: { ...rest, y: (y * grid) / prevGridY }
          }
        }, {})
      })
      requestAnimationFrame(() => {
        document.body.attributeStyleMap.set('--playlist-grid-y', grid)
      })
      return
    }
  }
}
