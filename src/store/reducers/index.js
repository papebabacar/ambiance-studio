import S from 's-js'
// import { composeWithDevtools } from '@reduce-devtools'
import root from './root'
import midi from './midi'
import playback from './playback'
import recording from './recording'
import notes from './notes/index'
import generators from './generators'
import patterns from './patterns'
import tracks from './tracks'
import grid from './grid'
import views from './views'
import tools from './tools'

const reducers = [root, midi, playback, recording, notes, generators, patterns, grid, tracks, views, tools]

const rootReducer = (state, action) => {
  reducers.map((reduce) => reduce(state, action))
  return Object.entries(state).reduce(
    (state, [prop, value]) => ({
      ...state,
      [prop]: typeof value === 'function' ? S.sample(value) : value
    }),
    {}
  )
}

export default rootReducer
