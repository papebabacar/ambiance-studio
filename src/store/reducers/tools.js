import S from 's-js'
import {
  TOOL_POINTER,
  TOOL_PAINTER,
  TOOL_SELECTION,
  TOOL_DELETE,
  TOOL_MUTE,
  TOOL_CUT,
  PIANOROLL_VIEW
} from '@store/actions'

export default (state, action) => {
  const store = S.sample(state.view) === PIANOROLL_VIEW ? state.pianoroll : state.playlist
  const { type } = action
  switch (type) {
    case TOOL_POINTER: {
      store({ ...store(), action: TOOL_POINTER })
      return
    }
    case TOOL_PAINTER: {
      store({ ...store(), action: TOOL_PAINTER })
      return
    }
    case TOOL_SELECTION: {
      store({ ...store(), action: TOOL_SELECTION })
      return
    }
    case TOOL_DELETE: {
      store({ ...store(), action: TOOL_DELETE })
      return
    }
    case TOOL_MUTE: {
      store({ ...store(), action: TOOL_MUTE })
      return
    }
    case TOOL_CUT: {
      store({ ...store(), action: TOOL_CUT })
      return
    }
  }
}
