import { UNDO, REDO } from '@store/actions'

import { History } from '@store/history'

export default (state, action) => {
  const { type } = action
  switch (type) {
    case UNDO: {
      History.undo(state)
      return
    }
    case REDO: {
      History.redo(state)
      return
    }
  }
}
