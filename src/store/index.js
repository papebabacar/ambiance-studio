// import { initDevtools } from '@reduce-devtools'
// import isEqual from 'lodash/isEqual'
import S from 's-js'
import reduce from './reducers'
import store from './store'
// import { Doc, Map } from 'yjs'
// import { WebsocketProvider } from 'y-websocket'

export * from './actions'

// const doc = new Doc()
// const ystate = doc.get('documen', Map)
// new WebsocketProvider('ws://localhost:1234', 'roomname', doc)
// const watchedProps = [
//   'microsecondsPQN',
//   'numerator',
//   'denominator',
//   'patterns',
//   'mutedTracks',
//   'notes',
//   'clips',
//   'generators',
//   'effects',
//   'selectedGenerator',
//   'selectedEffect',
//   'selectedPattern'
// ]

// doc.transact(() => {
//   watchedProps.map(key => {
//     ystate.set(key, store[key])
//   })
// })

// doc.on('update', () => {
//   const setValue = (state, key, value) => (typeof state[key] === 'function' ? state[key](value) : (state[key] = value))
//   S.freeze(() => {
//     for (const [key, value] of ystate) {
//       !isEqual(value, state[key]()) && setValue(state, key, value)
//       // setValue(state, key, value)
//     }
//     // console.log(state.selectedGenerator())
//   })
// })

class Store {
  static dispatch(action, payload = {}, silent) {
    Store.subscribe(state, { type: action, payload, silent })
  }

  static subscribe(state, action = {}) {
    reduce(state, action)
  }
}

const standardDispatch = ({ type: action, payload, silent }) => {
  // switch (action) {
  //   case '__REDUX_DEVTOOLS_EXTENSION_SET_STATE__': {
  //     Object.entries(payload).map(([prop, value]) => {
  //       typeof state[prop] === 'function' && state[prop](value)
  //     })
  //   }
  // }
  Store.dispatch(action, payload, silent)
}

if (process.env.NODE_ENV === 'development') {
  // initDevtools(store, standardDispatch, { name: 'Music store' })
}

const state = {}

Object.entries(store).map(([prop, value]) => (state[prop] = S.value(value)))

state.added = {}
state.deleted = {}
state.dirty = {}

const dispatch = Store.dispatch

export { state, dispatch }
