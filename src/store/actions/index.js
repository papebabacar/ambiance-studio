export const PLAY = 'PLAY'
export const PAUSE = 'PAUSE'
export const STOP = 'STOP'
export const RECORD = 'RECORD'
export const SEEK = 'SEEK'
export const UNDO = 'UNDO'
export const REDO = 'REDO'
export const SET_TEMPO = 'SET_TEMPO'
export const SET_NUMERATOR = 'SET_NUMERATOR'
export const SET_DENOMINATOR = 'SET_DENOMINATOR'
export const SET_PIANOROLL_SNAP = 'SET_PIANOROLL_SNAP'
export const SET_PLAYLIST_SNAP = 'SET_PLAYLIST_SNAP'
export const SET_PIANOROLL_GRID_X = 'SET_PIANOROLL_GRID_X'
export const SET_PLAYLIST_GRID_X = 'SET_PLAYLIST_GRID_X'
export const SET_PIANOROLL_GRID_Y = 'SET_PIANOROLL_GRID_Y'
export const SET_PLAYLIST_GRID_Y = 'SET_PLAYLIST_GRID_Y'
export const NOTE_ON = 'NOTE_ON'
export const NOTE_OFF = 'NOTE_OFF'
export const NOTE_ADD = 'NOTE_ADD'
export const NOTE_RECORD_ON = 'NOTE_RECORD_ON'
export const NOTE_RECORD_OFF = 'NOTE_RECORD_OFF'
export const NOTE_MOVE = 'NOTE_MOVE'
export const NOTE_RESIZE = 'NOTE_RESIZE'
export const NOTE_DELETE = 'NOTE_DELETE'
export const NOTE_MUTE = 'NOTE_MUTE'
export const NOTE_SELECT = 'NOTE_SELECT'
export const NOTE_UNSELECT = 'NOTE_UNSELECT'
export const NOTE_DEFAULT = 'NOTE_DEFAULT'
export const SELECTION_MOVE = 'SELECTION_MOVE'
export const SELECTION_RESIZE = 'SELECTION_RESIZE'
export const SILENCE = 'SILENCE'
export const TOOL_POINTER = 'TOOL_POINTER'
export const TOOL_PAINTER = 'TOOL_PAINTER'
export const TOOL_SELECTION = 'TOOL_SELECTION'
export const TOOL_DELETE = 'TOOL_DELETE'
export const TOOL_MUTE = 'TOOL_MUTE'
export const TOOL_CUT = 'TOOL_CUT'
export const TOOL_QUANTIZE = 'TOOL_QUANTIZE'
export const TOOL_CHOP = 'TOOL_CHOP'
export const PIANOROLL_VIEW = 'PIANOROLL_VIEW'
export const PLAYLIST_VIEW = 'PLAYLIST_VIEW'
export const SONG_LOOP = 'SONG_LOOP'
export const SONG_MODE = 'SONG_MODE'
export const TRACK_MUTE = 'TRACK_MUTE'
export const TRACK_UNMUTE = 'TRACK_UNMUTE'
export const GENERATOR_ADD = 'GENERATOR_ADD'
export const GENERATOR_DELETE = 'GENERATOR_DELETE'
export const GENERATOR_MUTE = 'GENERATOR_MUTE'
export const GENERATOR_SELECT = 'GENERATOR_SELECT'
export const PATTERN_ADD = 'PATTERN_ADD'
export const PATTERN_DELETE = 'PATTERN_DELETE'
export const PATTERN_SELECT = 'PATTERN_SELECT'
export const CONTROL_SET = 'CONTROL_SET'
export const CONTROL_VIEW = 'CONTROL_VIEW'
