import cloneDeep from 'lodash/cloneDeep'
// import isEqual from 'lodash/isEqual'
// import { doc } from '@store'
// import { Map, UndoManager } from 'yjs'

let undoManager
let history = []
let current = 0
const watchedProps = [
  'microsecondsPQN',
  'numerator',
  'denominator',
  'patterns',
  'mutedTracks',
  'notes',
  'clips',
  'generators',
  'effects',
  'added',
  'deleted',
  'dirty',
  'selectedGenerator',
  'selectedEffect',
  'selectedPattern'
]

export class History {
  static undo(state) {
    // undoManager && undoManager.undo()
    if (current !== 0 && !history[current]) {
      history[current] = Object.keys(history[current - 1]).reduce(
        (props, key) => ({ ...props, [key]: getValue(state, key) }),
        {}
      )
    }
    if (history[current - 1]) {
      for (const [key, value] of Object.entries(history[--current])) {
        history[current][key] !== history[current + 1][key] && setValue(state, key, value)
      }
    }
  }
  static redo(state) {
    undoManager && undoManager.redo()
    if (history[current + 1]) {
      for (const [key, value] of Object.entries(history[++current])) {
        history[current][key] !== history[current - 1][key] && setValue(state, key, value)
      }
    }
  }
  static remember(state) {
    // doc.transact(() => {
    //   const ystate = doc.get('documen', Map)
    //   const oldValues = Array.from(ystate.keys()).reduce((snapshots, key) => {
    //     return { ...snapshots, [key]: getValue(state, key) }
    //   }, {})
    //   if (!undoManager) {
    //     for (const key of ystate.keys()) {
    //       ystate.set(key, getValue(state, key))
    //     }
    //     undoManager = new UndoManager(ystate)
    //   }
    if (current === 0 || history[current - 1]) {
      const newHistory = watchedProps.reduce((newState, key) => {
        return { ...newState, [key]: getValue(state, key) }
      }, {})
      history[current++] = newHistory
      history[current] && (history = history.splice(0, current))
    }
    // setImmediate(() => {
    //   for (const key of ystate.keys()) {
    //     const newValue = getValue(state, key)
    //     !isEqual(oldValues[key], newValue) && ystate.set(key, newValue)
    //   }
    // })
    // })
  }

  static forget() {
    if (history[current - 1]) {
      delete history[--current]
    }
  }

  static markAsAdded(id) {
    history[current - 1].added[id] = true
  }

  static markAsDeleted(id) {
    history[current - 1].deleted[id] = true
  }

  static markAsDirty(id) {
    history[current - 1].dirty[id] = true
  }
}

const getValue = (state, key) => (typeof state[key] === 'function' ? cloneDeep(state[key]()) : cloneDeep(state[key]))

const setValue = (state, key, value) => (typeof state[key] === 'function' ? state[key](value) : (state[key] = value))
