import S from 's-js'
import { PianorollManager } from '@core/graphics/pianoroll'
import { dispatch, SET_PIANOROLL_SNAP, SET_PLAYLIST_SNAP, state } from '@store'

S.root(() => {
  S(() => {
    document.body.attributeStyleMap.set('--numerator', state.numerator())
    document.body.attributeStyleMap.set('--denominator', state.denominator())
    // dispatch(SET_PIANOROLL_SNAP, { snap: S.sample(state.pianoroll).snapType })
    // dispatch(SET_PLAYLIST_SNAP, { snap: S.sample(state.playlist).snapType })
  })
  S(() => {
    Object.entries(state.playingNotes()).map(([noteNumber, { on = 0, off = 0 }]) => {
      if (on !== null) {
        PianorollManager.noteOn(noteNumber, on)
      }
      if (off !== null) {
        PianorollManager.noteOff(noteNumber, off)
      }
    })
  })
})
