const statesBuffer = new self.SharedArrayBuffer(8)

const states = new Int32Array(statesBuffer)

postMessage({ type: 'INIT', states })

self.Atomics.store(states, 0, 0)

const process = (previousInterval) => {
  previousInterval && clearInterval(previousInterval)
  self.Atomics.wait(states, 0, 0)
  postMessage({ type: 'PROCESS' })
  const interval = setInterval(() => process(interval), (128 / 44100) * 1000)
}

process()
