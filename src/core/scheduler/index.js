import { CLOCK_AHEAD, CLOCK_STEP } from '@constants'
import { state, dispatch, NOTE_ON, NOTE_OFF, SILENCE } from '@store'
import { barInSeconds, pxInSeconds, secondsInPx, getPianorollSnapWidth, getFrequency } from '@utils/midi'
import { hitCheckX, roundToNearest, snapLength } from '@utils'

let timerNode
let currentStep = 0
let delay = 0.0
let renderedLoops = 0
let songLoops = 0
let startTime = 0.0
let rendered = 0.0

export class Scheduler {
  static async init({ timerNode: audioNode, states }) {
    timerNode = audioNode
    timerNode.port.postMessage({ type: 'INIT', states })
    this.states = states
  }

  static pxInSeconds(px, isClip = true) {
    const numerator = state.numerator()
    const denominator = state.denominator()
    const store = isClip ? state.playlist : state.pianoroll
    const { gridX } = store()
    const barX = isClip ? gridX : (gridX * numerator * 16) / denominator
    return px * (barInSeconds() / barX)
  }

  static secondsInPx(seconds, isClip = true) {
    const numerator = state.numerator()
    const denominator = state.denominator()
    const store = isClip ? state.playlist : state.pianoroll
    const { gridX } = store()
    const barX = isClip ? gridX : (gridX * numerator * 16) / denominator
    return seconds / (barInSeconds() / barX)
  }

  static songStart() {
    const store = state.songMode() ? state.playlist : state.pianoroll
    const { start } = store()
    return start ? pxInSeconds(start) : 0
  }

  static songDuration() {
    const store = state.songMode() ? state.playlist : state.pianoroll
    const { end, length } = store()
    return end ? pxInSeconds(end) : snapLength(pxInSeconds(length), barInSeconds())
  }

  static play() {
    // timerNode.port.postMessage({ type: 'PLAY' })
    self.Atomics.store(this.states, 0, 1)
    self.Atomics.notify(this.states, 0, 1)
    self.Atomics.store(Scheduler.states, 1, 0)
    this.context.resume()
  }

  static pause() {
    startTime = 0.0
    rendered = 0.0
    // dispatch(SILENCE)
    // timerNode.port.postMessage({ type: 'STOP' })
    self.Atomics.store(this.states, 0, 0)
    self.Atomics.store(Scheduler.states, 1, 0)
  }

  static stop() {
    startTime = 0.0
    rendered = 0.0
    // dispatch(SILENCE)
    // timerNode.port.postMessage({ type: 'STOP' })
    self.Atomics.store(this.states, 0, 0)
    self.Atomics.store(Scheduler.states, 1, 0)
    state.cursorX(secondsInPx(Scheduler.songStart()))
    state.patternCursorX(null)
  }

  static seek(cursorX) {
    startTime = 0.0
    rendered = 0.0
    self.Atomics.store(Scheduler.states, 1, 0)
    const snapCursorX = Math.min(
      secondsInPx(Scheduler.songDuration()),
      Math.max(secondsInPx(Scheduler.songStart()), cursorX)
    )
    state.cursorX(snapCursorX)
    // dispatch(SILENCE)
  }

  static get cursorX() {
    return secondsInPx(delay + this.context.currentTime - startTime)
  }

  static setInstrument(instrument, globalGain) {
    this.instrument = instrument
    this.globalGain = globalGain
  }

  static process() {
    const currentTime = this.context.currentTime
    if (startTime && delay + currentTime - startTime >= Scheduler.songDuration()) {
      startTime = currentTime
      delay = Scheduler.songStart()
      songLoops++
    }
    while (this.context.currentTime >= rendered - CLOCK_AHEAD && self.Atomics.load(Scheduler.states, 0)) {
      schedule(currentStep, state.cursorX())
    }
  }
}

const getStepTime = () => {
  return CLOCK_STEP
  // const duration = Math.abs(Scheduler.songDuration() - delay)
  // const clockStep = CLOCK_STEP >= duration ? CLOCK_STEP : CLOCK_STEP
  // return duration === 0 ? clockStep : duration / Math.floor(duration / clockStep)
}

const next = (songTime) => {
  currentStep++
  rendered += getStepTime()
  state.cursorX(secondsInPx(songTime))
}

const schedule = () => {
  const currentTime = Scheduler.context.currentTime
  const stepTime = getStepTime()
  if (!self.Atomics.load(Scheduler.states, 1)) {
    const cursorTime = pxInSeconds(state.cursorX())
    delay = cursorTime
    // const snapTime = roundToNearest(cursorTime, stepTime)
    currentStep = Math.floor(cursorTime / stepTime)
    self.Atomics.store(Scheduler.states, 1, 1)
    renderedLoops = 0
    songLoops = 0
    // state.cursorX(secondsInPx(cursorTime))
    rendered = currentTime
    startTime = currentTime
  }
  const songTime = delay + currentTime - startTime

  Scheduler.context.currentTime > rendered && console.warn('oouch!!')
  if (state.songMode()) {
    scheduleClips(songTime)
  } else {
    const { mute } = state.generators()[state.selectedGenerator()]
    const { notes } = state.patterns()[state.selectedPattern()]
    !mute && scheduleNotes(songTime, Object.keys(notes))
  }
  metronome(songTime)

  next(songTime)
  if (currentStep * stepTime >= Scheduler.songDuration()) {
    currentStep = Math.floor(Scheduler.songStart() / stepTime)
    renderedLoops++
  }
}

const scheduleClips = (songTime) => {
  const activeClips = Object.values(state.clips()).reduce((clips, clip) => {
    const { mute, x, offset, width, pattern } = clip
    const { generator } = state.patterns()[pattern]
    const { mute: generatorMute } = state.generators()[generator]
    if (mute || generatorMute) return clips
    if (pattern === state.selectedPattern()) {
      const start = Scheduler.pxInSeconds(x)
      const duration =
        Scheduler.pxInSeconds(width) - Math.max(0, start + Scheduler.pxInSeconds(width) - Scheduler.songDuration())
      if (songTime >= start && songTime < start + duration) {
        const patternTime = songTime - Scheduler.pxInSeconds(x - offset)
        state.patternCursorX(Scheduler.secondsInPx(patternTime, false))
      }
    }
    return isSchedulable(clip) ? { ...clips, [pattern]: clips[pattern] ? [...clips[pattern], clip] : [clip] } : clips
  }, {})
  const stepTime = currentStep * getStepTime()
  Object.entries(activeClips).map(([pattern, clips]) => {
    let mostRecentTime = null
    let mostRecentIndex = 0
    clips.map((clip, index) => {
      const { x } = clip
      const clipTime = pxInSeconds(x)
      if (mostRecentTime) {
        if (stepTime - clipTime < mostRecentTime) mostRecentIndex = index
      } else {
        mostRecentIndex = index
      }
    })
    const mostRecentClip = clips[mostRecentIndex]
    scheduleNotes(songTime, Object.keys(state.patterns()[pattern].notes), mostRecentClip)
  })
}

const scheduleNotes = (songTime, ids, clip = {}) => {
  const {
    x: clipX = 0,
    y: clipY,
    width: clipWidth = getPianorollSnapWidth('BAR'),
    offset: clipOffset = 0,
    pattern = state.selectedPattern()
  } = clip
  const stepTime = getStepTime()
  const schedulerStart = currentStep * stepTime
  const schedulerDuration = stepTime - Math.max(0, schedulerStart + stepTime - Scheduler.songDuration())
  ids.map((id) => {
    const note = state.notes()[id]
    const { mute, x, y, width } = note
    if (mute) return
    const noteStart = Scheduler.pxInSeconds(x, false) + Scheduler.pxInSeconds(clipX - clipOffset, true)
    const noteDuration =
      Scheduler.pxInSeconds(width, false) -
      Math.max(0, noteStart + Scheduler.pxInSeconds(width, false) - Scheduler.songDuration())
    const schedulable = hitCheckX(
      { x: schedulerStart, width: schedulerDuration },
      { x: noteStart, width: noteDuration }
    )
    if (!schedulable) return

    const generator = state.patterns()[pattern].generator
    const track = clipY === undefined ? undefined : Math.floor(clipY / state.playlist().gridY)
    const duration = Scheduler.songDuration() * Math.abs(renderedLoops - songLoops)
    const noteNumber = y / state.pianoroll().gridY
    if (state.scheduledNotes()[y] === undefined) {
      const time = Math.max(0, noteStart - songTime + duration)
      state.scheduledNotes()[y] = 1
      const frequency = getFrequency(noteNumber * state.pianoroll().gridY)
      // Scheduler.instrument.parameters.get('frequency').setValueAtTime(frequency, Scheduler.context.currentTime + time)
      // Scheduler.globalGain.gain.setValueAtTime(0.5, Scheduler.context.currentTime + time)
      // Scheduler.globalGain.gain.setValueAtTime(0, Scheduler.context.currentTime + time + 0.125)
      dispatch(NOTE_ON, { noteNumber, time, generator, track })
    }
    if (
      state.scheduledNotes()[y] &&
      schedulerStart + schedulerDuration >=
        Math.min(noteStart + noteDuration, Scheduler.pxInSeconds(clipX + clipWidth, true))
    ) {
      const time = Math.max(
        0,
        Math.min(noteStart + noteDuration, Scheduler.pxInSeconds(clipX + clipWidth, true)) - songTime + duration
      )
      dispatch(NOTE_OFF, { noteNumber, time, generator, track })
      // Scheduler.globalGain.gain.setValueAtTime(0, Scheduler.context.currentTime + time)
      state.scheduledNotes()[y] = undefined
    }
  })
}

const metronome = (songTime) => {
  // const stepTime = getStepTime()
  // const duration = Scheduler.songDuration() - delay
  // const beatInSeconds = barInSeconds() / ((state.numerator() * 16) / state.denominator())
  // for (let i = 0; i < 4; i++) {
  //   if (hitCheckX({ x: currentStep * stepTime, width: stepTime }, { x: i * 4 * beatInSeconds, width: beatInSeconds })) {
  //     const time = i * 4 * beatInSeconds + loops * duration - songTime
  //     const osc = Scheduler.context.createOscillator()
  //     osc.connect(Scheduler.context.destination)
  //     osc.frequency.value = 440
  //     osc.start(Scheduler.context.currentTime + time)
  //     osc.stop(Scheduler.context.currentTime + time + 0.08)
  //   }
  // }
}

const isSchedulable = (clip, parentX = 0, parentOffset = 0) => {
  if (clip.mute) return
  const stepTime = getStepTime()
  const schedulerStart = currentStep * stepTime
  const { x, width } = clip
  const noteStart = Scheduler.pxInSeconds(x, true) + Scheduler.pxInSeconds(parentX - parentOffset, true)
  const noteDuration =
    Scheduler.pxInSeconds(width, true) -
    Math.max(0, noteStart + Scheduler.pxInSeconds(width, true) - Scheduler.songDuration())
  const schedulerDuration = stepTime - Math.max(0, schedulerStart + stepTime - Scheduler.songDuration())
  return hitCheckX({ x: schedulerStart, width: schedulerDuration }, { x: noteStart, width: noteDuration })
}
