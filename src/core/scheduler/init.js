import { Scheduler } from './index'
import TimerWorklet from './timer.worklet'
import Worker from './timer.worker'
;(async () => {
  const context = new AudioContext({ sampleRate: 44100 })
  Scheduler.context = context
  await context.audioWorklet.addModule(TimerWorklet)
  const timerNode = new AudioWorkletNode(context, 'timer-processor')
  //   timerNode.connect(context.destination)
  //   timerNode.port.onmessage = Scheduler.process.bind(Scheduler)
  const worker = Worker()
  worker.addEventListener('message', ({ data }) => {
    const { type } = data
    if (type === 'INIT') {
      const { states } = data
      Scheduler.init({ timerNode, states })
    }
    if (type === 'PROCESS') {
      // const { states } = data
      Scheduler.process()
    }
  })
})()
