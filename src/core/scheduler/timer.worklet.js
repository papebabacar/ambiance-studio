// eslint-disable-next-line no-undef
class TimerProcessor extends AudioWorkletProcessor {
  constructor() {
    super()
    this.port.onmessage = ({ data }) => {
      const { type } = data
      if (type === 'INIT') {
        const { states } = data
        this._initialized = true
        this._states = states
      }
    }
  }
  process() {
    // if (!this._initialized) return true
    // const started = Atomics.load(this._states, 0)
    // started && this.port.postMessage(true)
    return true
  }
}
registerProcessor('timer-processor', TimerProcessor)
