import { Plugin } from './plugin'
let node

export class MidiPlugin extends Plugin {
  async load() {}

  play(time) {
    node.play(time)
  }

  stop(time) {
    node.stop(time)
  }

  get clipWidth() {
    return 40
  }
}
