import { Plugin } from './plugin'
import sound from '@assets/kick.wav'
import extractPeaks from 'webaudio-peaks'
import { graph } from '@core/audio'
import { barInSeconds } from '@utils/midi'
import { state } from '@store'

let audioBuffer
let node
let peaks

export class WavePlugin extends Plugin {
  async load() {
    const mp3File = await fetch(sound)
    const audioData = await mp3File.arrayBuffer()
    audioBuffer = await graph.context.decodeAudioData(audioData)
    const samplesPerPixel = (graph.context.sampleRate * barInSeconds()) / state.playlist().gridX
    peaks = extractPeaks(audioBuffer, samplesPerPixel, 8)
    node = graph.addSource('Sample', 'buffer', audioBuffer)
    const gain = node.addNode('gain')
    gain.connectToDestination()
  }

  play(time) {
    node.play(time)
  }

  stop(time) {
    node.stop(time)
  }

  get peaks() {
    return peaks
  }

  get clipWidth() {
    return 100
  }

  static drawNote($note) {
    const canvas = $note.querySelector('canvas') || document.createElement('canvas')
    const maxValue = 2 ** (peaks.bits - 1)
    const ctx = canvas.getContext('2d', {
      desynchronized: true,
      preserveDrawingBuffer: true
    })
    const h2 = (state.playlist().gridY - 18) / 2
    canvas.width = peaks.length
    canvas.height = h2 * 2
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.strokeStyle = '#ecefec'
    ctx.beginPath()
    for (let i = 0; i < peaks.length; i += 1) {
      const minPeak = peaks.data[0][i * 2] / maxValue
      const maxPeak = peaks.data[0][i * 2 + 1] / maxValue
      const min = Math.abs(minPeak * h2)
      const max = Math.abs(maxPeak * h2)
      ctx.moveTo(i, h2 - max)
      ctx.lineTo(i, h2 + min)
    }
    ctx.stroke()
    $note.append(canvas)
    return $note
  }
}
