import { state } from '@store'
import { WavePlugin } from './Wave'
import S from 's-js'
import { toPlaylist } from '@utils/midi'

let plugins = {}

export class PluginManager {
  static async loadPlugin(id) {
    const plugin = new WavePlugin()
    await plugin.load()
    plugins[id] = plugin
  }
  // static updateTime(factorWidth, factorOffset) {
  //   plugins[state.selectedGenerator()].updateTime(factorWidth, factorOffset)
  // }
  static get activePlugin() {
    return plugins[S.sample(state.selectedGenerator)]
  }
  static get clipWidth() {
    return plugins[S.sample(state.selectedGenerator)].clipWidth
  }
  static startRecord() {
    // return plugins[S.sample(state.selectedGenerator)].clipWidth
  }
  static stopRecord() {
    // return plugins[S.sample(state.selectedGenerator)].clipWidth
  }
  static drawNote($container, { pattern }) {
    if (!$container) return
    console.time('draw')
    const { gridX, gridY } = S.sample(state.playlist)
    const { notes, length, minY, maxY } = S.sample(state.patterns)[pattern]
    const factorY = ((gridY - 18) / (maxY - minY + gridY)) * (gridY / gridX)
    let canvas = $container.querySelector('canvas')
    if (!canvas) {
      canvas = document.createElement('canvas')
      $container.append(canvas)
    }
    const ctx = canvas.getContext('2d', {
      desynchronized: true,
      preserveDrawingBuffer: true
    })
    canvas.width = toPlaylist(length)
    canvas.height = gridY - 18
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.fillStyle = '#aaa'
    ctx.beginPath()
    Object.keys(notes).map(id => {
      const { x, y, width } = S.sample(state.notes)[id]
      ctx.fillRect(
        toPlaylist(x),
        (y - minY) * factorY + Math.max(0, (gridY - (maxY - minY) * factorY) / 4),
        toPlaylist(width),
        2
      )
    })
    ctx.stroke()
    console.timeEnd('draw')
  }
}
