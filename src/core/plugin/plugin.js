export class Plugin {
  async load() {
    throw new Error('This method must be implemented')
  }

  play() {
    throw new Error('This method must be implemented')
  }

  stop() {
    throw new Error('This method must be implemented')
  }

  get clipWidth() {
    throw new Error('The clipWidth getter must be implemented')
  }
}
