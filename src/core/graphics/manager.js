import S from 's-js'
import { mix } from '@utils'
import { pxInSeconds } from '@utils/midi'
import services from './services'

const { RootService, ...restServices } = services

console.log(services)

export class Manager extends mix(RootService).with(...Object.values(restServices)) {
  static get state() {
    return () => {
      throw new Error('This method must be implemented')
    }
  }

  static get notes() {
    return () => {
      throw new Error('This method must be implemented')
    }
  }

  static drawNote() {
    return () => {
      throw new Error('This method must be implemented')
    }
  }

  static get leftResizable() {
    return false
  }

  static get filterByPattern() {
    return false
  }

  static setSize() {
    return () => {
      throw new Error('This method must be implemented')
    }
  }

  static getSnapWidth() {
    return () => {
      throw new Error('This method must be implemented')
    }
  }

  static songStart() {
    const { start } = S.sample(this.state)
    return start ? pxInSeconds(start) : 0
  }

  static set container(container) {
    this.$container = container
    container.onscroll = () => {
      this.setSize(this.$container.scrollLeft + this.$container.offsetWidth + 3500, container.scrollTop + 300)
    }
  }

  static set editor(editor) {
    this.$editor = editor
    this.$notes = {}
    this.setupEditor()
    this.render()
  }

  static set timeline(timeline) {
    this.$timeline = timeline
    this.setupTimeline()
  }

  static set highlights(highlights) {
    this.$highlights = highlights
    this.setupHighlights()
  }

  static set minimap(minimap) {
    this.$minimap = minimap
  }
}
