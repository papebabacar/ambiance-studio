import S from 's-js'
import { state, dispatch, SELECTION_MOVE, PIANOROLL_VIEW, NOTE_ON, NOTE_OFF, NOTE_MOVE } from '@store'
import { History } from '@store/history'
import { clearMouseEvents } from '@utils'

export default (superclass) =>
  class extends superclass {
    static startMoveNote(id, offsetX) {
      let moved
      History.remember(state)
      window.onmousemove = (event) => {
        const { snap, gridY } = S.sample(this.state)
        const { x: prevX, y: prevY } = this.activeNotes[id]
        const offsetGrid = Math.round(((prevX / snap) % 1) * snap)
        const x = event.clientX - offsetGrid - this.$editor.getBoundingClientRect().left + window.scrollX
        const y = Math.max(
          0,
          Math.min(
            this.$editor.offsetHeight - gridY,
            event.clientY - this.$editor.getBoundingClientRect().top + window.scrollY
          )
        )
        this.scrollIntoView(x, y)
        const snapX = Math.max(0, x - (x % snap) - (offsetX - (offsetX % snap)) + offsetGrid)
        const snapY = y - (y % gridY)
        this.$notes[id].attributeStyleMap.set('cursor', 'move')
        this.$editor.attributeStyleMap.set('cursor', 'move')
        if (this.selection[id]) {
          let dx = snapX - prevX
          let dy = snapY - prevY
          for (const id of Object.keys(this.selection)) {
            if (this.activeNotes[id].x + dx < 0) {
              dx = -this.activeNotes[id].x
            }
            if (this.activeNotes[id].y + dy < 0) {
              dy = -this.activeNotes[id].y
            }
            if (this.activeNotes[id].y + dy > this.$editor.offsetHeight - gridY) {
              dy = this.$editor.offsetHeight - gridY - this.activeNotes[id].y
            }
          }
          if (dx !== 0 || dy !== 0) {
            moved = true
            dispatch(SELECTION_MOVE, { ids: Object.keys(this.selection), dx, dy })
          }
        } else {
          if (snapY !== prevY) {
            S.sample(state.view) === PIANOROLL_VIEW &&
              dispatch(NOTE_OFF, { noteNumber: prevY / S.sample(this.state).gridY })
            S.sample(state.view) === PIANOROLL_VIEW &&
              dispatch(NOTE_ON, { noteNumber: snapY / S.sample(this.state).gridY })
          }
          if (snapX !== prevX || snapY !== prevY) {
            moved = true
            dispatch(NOTE_MOVE, { id, x: snapX, y: snapY })
          }
        }
      }
      window.onmouseup = (event) => {
        const { gridY } = S.sample(this.state)
        const y = Math.max(
          0,
          Math.min(
            this.$editor.offsetHeight - gridY,
            event.clientY - this.$editor.getBoundingClientRect().top + window.scrollY
          )
        )
        const snapY = y - (y % gridY)
        !moved && History.forget()
        this.$editor.attributeStyleMap.set('cursor', 'auto')
        S.sample(state.view) === PIANOROLL_VIEW &&
          dispatch(NOTE_OFF, { noteNumber: snapY / S.sample(this.state).gridY })
        clearMouseEvents()
      }
    }
  }
