import S from 's-js'
import { dispatch, NOTE_MUTE } from '@store'
import { clearMouseEvents } from '@utils'
import { checkIntersection } from 'line-intersect'
import { History } from '@store/history'

export default (superclass) =>
  class extends superclass {
    static startMuteNote() {
      let pointerStart
      window.onmousemove = ({ clientX, clientY }) => {
        const { gridY } = S.sample(this.state)
        const pointerX = Math.max(0, clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
        const pointerY = Math.max(0, clientY - this.$editor.getBoundingClientRect().top + window.scrollY)
        this.scrollIntoView(pointerX, pointerY)
        if (!pointerStart) pointerStart = { x: pointerX, y: pointerY }
        const ids = Object.entries(this.activeNotes).reduce((notes, [id, note]) => {
          const { x, y, width, mute: noteMute } = note
          const { type: typeT } = checkIntersection(
            x,
            y,
            x + width,
            y,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          const { type: typeR } = checkIntersection(
            x + width,
            y,
            x + width,
            y + gridY,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          const { type: typeB } = checkIntersection(
            x,
            y + gridY,
            x + width,
            y + gridY,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          const { type: typeL } = checkIntersection(
            x,
            y,
            x,
            y + gridY,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          if (
            typeT === 'intersecting' ||
            typeR === 'intersecting' ||
            typeB === 'intersecting' ||
            typeL === 'intersecting'
          ) {
            if (this.muteAction === null) this.muteAction = !noteMute
            return [...notes, id]
          } else {
            return notes
          }
        }, [])
        if (ids.length) {
          this.dirty = true
          dispatch(NOTE_MUTE, { ids, mute: this.muteAction })
        }
        pointerStart = { x: pointerX, y: pointerY }
      }
      window.onmouseup = () => {
        !this.dirty && History.forget()
        this.dirty = false
        this.muteAction = null
        clearMouseEvents()
      }
    }
  }
