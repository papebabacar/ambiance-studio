import S from 's-js'
import {
  state,
  dispatch,
  NOTE_ON,
  NOTE_OFF,
  NOTE_ADD,
  TOOL_POINTER,
  TOOL_PAINTER,
  TOOL_SELECTION,
  TOOL_DELETE,
  TOOL_MUTE,
  TOOL_CUT,
  PIANOROLL_VIEW
} from '@store'
import { History } from '@store/history'

export default (superclass) =>
  class extends superclass {
    static setupEditor() {
      this.$editor.onmousedown = ({ offsetX, offsetY }) => {
        const { action, gridY, snap } = S.sample(this.state)
        const x = offsetX - (offsetX % snap)
        const y = offsetY - (offsetY % gridY)
        if (action === TOOL_SELECTION) {
          History.remember(state)
          this.startSelection(true, x, y)
        }
        if (action === TOOL_POINTER) {
          S.sample(state.view) === PIANOROLL_VIEW && dispatch(NOTE_ON, { noteNumber: y / S.sample(this.state).gridY })
          this.scrollIntoView(x, y)
          History.remember(state)
          dispatch(NOTE_ADD, { x, y })
          this.unselectAll()
        }
        if (action === TOOL_PAINTER) {
          S.sample(state.view) === PIANOROLL_VIEW && dispatch(NOTE_ON, { noteNumber: y / S.sample(this.state).gridY })
          History.remember(state)
          this.startPaintNote({ x, y })
          this.unselectAll()
        }
        if (action === TOOL_DELETE) {
          History.remember(state)
          this.startDeleteNote()
        }
        if (action === TOOL_MUTE) {
          History.remember(state)
          this.startMuteNote()
        }
        if (action === TOOL_CUT) {
          History.remember(state)
          this.startCutNote()
        }
        if (S.sample(state.view) === PIANOROLL_VIEW) {
          this.$editor.onmouseleave = this.$editor.onmouseup = () => {
            dispatch(NOTE_OFF, { noteNumber: y / S.sample(this.state).gridY })
            this.$editor.onmouseleave = this.$editor.onmouseup = () => {}
          }
        }
      }
    }
    static setupHighlights() {
      const $songSelection = document.createElement('div')
      $songSelection.setAttribute('data-selection', true)
      this.$highlights.append($songSelection)
    }
  }
