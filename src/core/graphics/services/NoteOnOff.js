import S from 's-js'

export default (superclass) =>
  class extends superclass {
    static noteOn(noteNumber) {
      requestAnimationFrame(() => {
        // if (this.$notes[noteNumber]) {
        //   this.$notes[noteNumber].attributeStyleMap.set('opacity', 1)
        // } else {
        //   const { gridY } = S.sample(this.state)
        //   const div = document.createElement('div')
        //   div.attributeStyleMap.set('transform', `translate3d(0,${noteNumber * gridY}px,0)`)
        //   this.$notes[noteNumber] = div
        //   this.$highlights.append(div)
        // }
        this.drawNoteOn(noteNumber)
      })
    }

    static noteOff(noteNumber) {
      requestAnimationFrame(() => {
        // if (!this.$notes[noteNumber]) return
        // this.$notes[noteNumber].attributeStyleMap.set('opacity', 0)
        this.drawNoteOff(noteNumber)
      })
    }
  }
