import S from 's-js'
import { dispatch, NOTE_ADD, NOTE_RESIZE } from '@store'
import { History } from '@store/history'
import { checkIntersection } from 'line-intersect'
import { clearMouseEvents } from '@utils'

export default (superclass) =>
  class extends superclass {
    static startCutNote() {
      let pointerStart
      let pointerEnd
      window.onmousemove = (event) => {
        const pointerX = Math.max(0, event.clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
        const pointerY = Math.max(0, event.clientY - this.$editor.getBoundingClientRect().top + window.scrollY)
        this.scrollIntoView(pointerX, pointerY)
        if (!pointerStart) pointerStart = { x: pointerX, y: pointerY }
        pointerEnd = { x: pointerX, y: pointerY }
      }
      window.onmouseup = () => {
        let intersecting
        const { gridY } = S.sample(this.state)
        for (const [id, note] of Object.entries(this.activeNotes)) {
          const { x, y, offset, width: notewidth, mute, selected } = note
          const { type: typeT, point: pointT } = checkIntersection(
            x,
            y,
            x + notewidth,
            y,
            pointerStart.x,
            pointerStart.y,
            pointerEnd.x,
            pointerEnd.y
          )
          const { type: typeB, point: pointB } = checkIntersection(
            x,
            y + gridY,
            x + notewidth,
            y + gridY,
            pointerStart.x,
            pointerStart.y,
            pointerEnd.x,
            pointerEnd.y
          )
          if (typeT === 'intersecting' && typeB === 'intersecting' && pointT && pointB) {
            intersecting = true
            const width = Math.floor(Math.max(pointT.x, pointB.x) - Math.abs(pointT.x - pointB.x) / 2 - x)
            dispatch(NOTE_ADD, {
              x: x + width,
              y,
              offset: offset + width,
              width: notewidth - width,
              mute,
              selected
            })
            dispatch(NOTE_RESIZE, { id, width })
          }
        }
        !intersecting && History.forget()
        clearMouseEvents()
      }
    }
  }
