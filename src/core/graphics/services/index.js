import RootService from './RootService'
import SetupEditor from './SetupEditor'
import SetupTimeline from './SetupTimeline'
import Rendering from './Rendering'
import NoteOnOff from './NoteOnOff'
import AddNote from './AddNote'
import PaintNote from './PaintNote'
import DeleteNote from './DeleteNote'
import MuteNote from './MuteNote'
import CutNote from './CutNote'
import MoveNote from './MoveNote'
import ResizeNote from './ResizeNote'
import Selection from './Selection'

export default {
  RootService,
  SetupEditor,
  SetupTimeline,
  Rendering,
  NoteOnOff,
  AddNote,
  PaintNote,
  DeleteNote,
  MuteNote,
  CutNote,
  MoveNote,
  ResizeNote,
  Selection
}
