import S from 's-js'
export default class {
  static scrollIntoView(x, y, offset = 50) {
    if (x > this.$container.scrollLeft + this.$container.offsetWidth - offset * 2) {
      requestAnimationFrame(() => this.$container.scrollBy({ left: S.sample(this.state).gridX }))
    }
    if (x < this.$container.scrollLeft + offset) {
      requestAnimationFrame(() => this.$container.scrollBy({ left: -S.sample(this.state).gridX }))
    }
    if (y > this.$container.scrollTop + this.$container.offsetHeight - offset / 2) {
      requestAnimationFrame(() => this.$container.scrollBy({ top: S.sample(this.state).gridY }))
    }
    if (y < this.$container.scrollTop + offset / 2) {
      requestAnimationFrame(() => this.$container.scrollBy({ top: -S.sample(this.state).gridY }))
    }
    this.setSize(x, y)
  }
}
