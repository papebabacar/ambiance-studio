import S from 's-js'
import { dispatch, NOTE_SELECT, NOTE_UNSELECT } from '@store'
import { hitCheck, reverseRect, clearMouseEvents } from '@utils'
import { History } from '@store/history'

export default (superclass) =>
  class extends superclass {
    static selectAll() {
      for (const [id, { selected }] of Object.entries(this.activeNotes)) {
        if (!selected) {
          this.$notes[id].setAttribute('data-selected', true)
          dispatch(NOTE_SELECT, { id })
        }
      }
    }

    static unselectAll() {
      let selection
      for (const id of Object.keys(this.selection)) {
        selection = true
        this.$notes[id].setAttribute('data-selected', false)
        dispatch(NOTE_UNSELECT, { id })
      }
      return selection
    }

    static startSelection(unselect = true, startX, startY) {
      let selection = unselect ? this.unselectAll() : false
      let selStart = { x: startX, y: startY }
      const $selection = document.createElement('div')
      this.$editor.append($selection)
      window.onmousemove = (event) => {
        const { gridY } = S.sample(this.state)
        const x = Math.max(0, event.clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
        const y = Math.max(0, event.clientY - this.$editor.getBoundingClientRect().top + window.scrollY)
        this.scrollIntoView(x, y, 100)
        const { x1, y1, x2, y2 } = reverseRect(selStart, { x, y })
        $selection.setAttribute('data-selection', true)
        $selection.attributeStyleMap.set('transform', `translate3d(${x1}px,${y1}px,0)`)
        $selection.attributeStyleMap.set('width', CSS.px(x2 - x1))
        $selection.attributeStyleMap.set('height', CSS.px(y2 - y1))
        for (const [id, note] of Object.entries(this.activeNotes)) {
          const { x, y, width, selected: prevSelected } = note
          const selected = hitCheck({ x, y, width, height: gridY }, { x: x1, y: y1, width: x2 - x1, height: y2 - y1 })
          this.$notes[id].setAttribute('data-selected', selected)
          if (selected !== prevSelected) {
            selection = true
            selected ? dispatch(NOTE_SELECT, { id }) : dispatch(NOTE_UNSELECT, { id })
          }
        }
      }
      window.onmouseup = () => {
        !selection && History.forget()
        $selection.remove()
        clearMouseEvents()
      }
    }
  }
