import S from 's-js'
import { dispatch, NOTE_DELETE } from '@store'
import { clearMouseEvents } from '@utils'
import { checkIntersection } from 'line-intersect'
import { History } from '@store/history'

export default (superclass) =>
  class extends superclass {
    static deleteNote(id) {
      this.$notes[id].remove()
      this.$notes[id] = null
      if (this.filterByPattern) {
        this.$controllers[id].remove()
        this.$controllers[id] = null
      }
    }

    static startDeleteNote() {
      let pointerStart
      window.onmousemove = ({ clientX, clientY }) => {
        const { gridY } = S.sample(this.state)
        const pointerX = Math.max(0, clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
        const pointerY = Math.max(0, clientY - this.$editor.getBoundingClientRect().top + window.scrollY)
        this.scrollIntoView(pointerX, pointerY)
        if (!pointerStart) pointerStart = { x: pointerX, y: pointerY }
        const ids = Object.entries(this.activeNotes).reduce((notes, [id, note]) => {
          const { x, y, width } = note
          const { type: typeT } = checkIntersection(
            x,
            y,
            x + width,
            y,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          const { type: typeR } = checkIntersection(
            x + width,
            y,
            x + width,
            y + gridY,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          const { type: typeB } = checkIntersection(
            x,
            y + gridY,
            x + width,
            y + gridY,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          const { type: typeL } = checkIntersection(
            x,
            y,
            x,
            y + gridY,
            pointerStart.x,
            pointerStart.y,
            pointerX,
            pointerY
          )
          return typeT === 'intersecting' ||
            typeR === 'intersecting' ||
            typeB === 'intersecting' ||
            typeL === 'intersecting'
            ? [...notes, id]
            : notes
        }, [])
        if (ids.length) {
          this.deleted = true
          ids.map((id) => this.deleteNote(id))
          dispatch(NOTE_DELETE, { ids })
        }
        pointerStart = { x: pointerX, y: pointerY }
      }
      window.onmouseup = () => {
        !this.deleted && History.forget()
        this.deleted = false
        clearMouseEvents()
      }
    }
  }
