import S from 's-js'
import { state, dispatch, NOTE_RESIZE, SELECTION_RESIZE, NOTE_DEFAULT } from '@store'
import { History } from '@store/history'
import { clearMouseEvents } from '@utils'

export default (superclass) =>
  class extends superclass {
    static startResizeNote(id, isLeft) {
      let resized
      History.remember(state)
      window.onmousemove = (event) => {
        const minSnap = this.getSnapWidth('1/32BEAT')
        const { snap } = S.sample(this.state)
        const { x: prevX, y: pointerY, offset: prevOffset, width: prevWidth } = this.activeNotes[id]
        const offsetGrid = Math.round(((prevWidth / snap) % 1) * snap)
        const pointerX = Math.max(
          0,
          event.clientX - offsetGrid - this.$editor.getBoundingClientRect().left + window.scrollX
        )
        this.scrollIntoView(pointerX, pointerY)
        const width = this.leftResizable && isLeft ? prevWidth - offsetGrid * 2 - (pointerX - prevX) : pointerX - prevX
        const snapWidth = Math.max(
          Math.max(width + snap * 0.5 - ((width + snap * 0.5) % snap), offsetGrid > 0 ? 0 : snap) + offsetGrid,
          minSnap
        )
        const offset = this.leftResizable && isLeft ? prevWidth - snapWidth : 0
        this.$notes[id].attributeStyleMap.set('cursor', 'w-resize')
        this.$editor.attributeStyleMap.set('cursor', 'w-resize')
        if (this.selection[id]) {
          let dw = snapWidth - prevWidth
          for (const id of Object.keys(this.selection)) {
            const offsetGrid = Math.round(((this.activeNotes[id].width / snap) % 1) * snap)
            if (this.activeNotes[id].width + dw < Math.max(snap, minSnap)) {
              dw = offsetGrid > 0 ? dw : Math.max(snap, minSnap) - this.activeNotes[id].width
            } else if (this.leftResizable && isLeft && this.activeNotes[id].offset - dw < 0) {
              dw = 0
            }
          }
          if (dw !== 0) {
            resized = true
            dispatch(SELECTION_RESIZE, {
              ids: Object.keys(this.selection),
              dw,
              offset: this.leftResizable && isLeft ? -dw : 0
            })
          }
        } else {
          const newWidth = prevOffset + offset < 0 ? snapWidth + prevOffset + offset : snapWidth
          if (newWidth !== prevWidth) {
            resized = true
            dispatch(NOTE_RESIZE, {
              id,
              width: newWidth,
              offset: prevOffset + offset < 0 ? -prevOffset : offset
            })
          }
        }
      }
      window.onmouseup = () => {
        !resized && History.forget()
        const { width: currentWidth, offset: currentOffsetTime } = S.sample(this.notes)[id]
        dispatch(NOTE_DEFAULT, {
          currentWidth,
          currentOffsetTime
        })
        this.$editor.attributeStyleMap.set('cursor', 'auto')
        clearMouseEvents()
      }
    }
  }
