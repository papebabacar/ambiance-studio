import S from 's-js'
import { dispatch, NOTE_ADD } from '@store'
import { hitCheck, clearMouseEvents } from '@utils'

export default (superclass) =>
  class extends superclass {
    static startPaintNote({ x: startX, y: startY }) {
      const snapPaint = false
      const { snap, currentWidth, gridY } = S.sample(this.state)
      const minSnap = Math.max(snap, this.getSnapWidth('1/64BEAT'))
      const snapWidth = snapPaint ? minSnap : currentWidth
      dispatch(NOTE_ADD, { x: startX, y: startY, width: snapWidth })
      window.onmousemove = (event) => {
        const pointerX = Math.max(0, event.clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
        const direction = pointerX - startX > 0 ? 1 : -1
        const snapPointerX = pointerX - (pointerX % snapWidth) + snapWidth * direction
        this.scrollIntoView(pointerX, startY)
        const points = Array(Math.floor(Math.abs(snapPointerX - startX) / snapWidth))
          .fill()
          .reduce((notes, _, index) => {
            const x = startX + index * snapWidth * direction
            const snapX = x - (x % snap)
            let painted
            for (const note of Object.values(this.activeNotes)) {
              const { x, y, width, height = gridY } = note
              if (hitCheck({ x, y, width, height }, { x: snapX, y: startY, width: snapWidth, height })) {
                painted = true
              }
            }
            return painted ? notes : [...notes, { x: snapX, y: startY, width: snapWidth }]
          }, [])
        if (points.length) dispatch(NOTE_ADD, { points })
      }
      window.onmouseup = () => clearMouseEvents()
    }
  }
