import S from 's-js'
import {
  state,
  dispatch,
  TOOL_POINTER,
  TOOL_PAINTER,
  TOOL_SELECTION,
  TOOL_DELETE,
  TOOL_MUTE,
  TOOL_CUT,
  PIANOROLL_VIEW,
  NOTE_ON,
  NOTE_DEFAULT,
  NOTE_DELETE,
  NOTE_MUTE
} from '@store'
import { History } from '@store/history'
import pianorollStyle from '@components/Main/Pianoroll/style'
import playlistStyle from '@components/Main/Playlist/style'

export default (superclass) =>
  class extends superclass {
    static addNote({ id, x, y, width, mute, selected }) {
      if (this.$notes[id] && !state.dirty[id]) return false
      const handleWidth = Math.min(6, width / 2)
      const $note = this.$notes[id] ? this.$notes[id] : document.createElement('div')
      $note.id = id
      $note.attributeStyleMap.set('width', CSS.px(width))
      $note.setAttribute('data-mute', mute)
      $note.attributeStyleMap.set('transform', `translate3d(${x}px,${y}px,0)`)
      $note.setAttribute('data-selected', selected)
      $note.onmousedown = (event) => {
        event.stopPropagation()
        const { action } = S.sample(this.state)
        const { width: currentWidth, offset: currentOffsetTime, selected } = S.sample(this.notes)[id]
        if (action === TOOL_POINTER || action === TOOL_PAINTER) {
          if (event.offsetX >= width - handleWidth || (event.offsetX <= handleWidth && this.leftResizable)) {
            this.startResizeNote(id, event.offsetX <= handleWidth)
          } else {
            !selected &&
              S.sample(state.view) === PIANOROLL_VIEW &&
              dispatch(NOTE_ON, { noteNumber: y / S.sample(this.state).gridY })
            this.startMoveNote(id, event.offsetX)
          }
          !selected && this.unselectAll()
          dispatch(NOTE_DEFAULT, {
            currentWidth,
            currentOffsetTime
          })
        }
        if (action === TOOL_DELETE) {
          History.remember(state)
          this.deleted = true
          this.deleteNote(id)
          dispatch(NOTE_DELETE, { id })
          this.startDeleteNote()
        }
        if (action === TOOL_MUTE) {
          History.remember(state)
          this.dirty = true
          this.muteAction = !mute
          dispatch(NOTE_MUTE, { id, mute: !mute })
          this.startMuteNote()
        }
        if (action === TOOL_CUT) {
          History.remember(state)
          this.startCutNote()
        }
        if (action === TOOL_SELECTION) {
          History.remember(state)
          this.startSelection(false, x + event.offsetX, y + event.offsetY)
        }
      }
      $note.onmousemove = (event) => {
        const { action } = S.sample(this.state)
        if (action === TOOL_POINTER || action === TOOL_PAINTER) {
          event.offsetX >= width - handleWidth || (event.offsetX <= handleWidth && this.leftResizable)
            ? $note.attributeStyleMap.set('cursor', 'w-resize')
            : $note.attributeStyleMap.set('cursor', 'move')
        } else {
          $note.attributeStyleMap.set('cursor', 'auto')
        }
      }
      if (!this.$notes[id]) {
        if (this.filterByPattern) {
          $note.classList.add(pianorollStyle.note)
          this.$notes[this.selectedPattern].append($note)
          const $control = document.createElement('span')
          this.$controllers[this.selectedPattern].append($control)
          this.$controllers[id] = $control
        } else {
          this.$editor.append($note)
          $note.classList.add(playlistStyle.note)
        }
        this.$notes[id] = $note
      }

      requestAnimationFrame(() => this.drawNote($note, S.sample(this.notes)[id]))

      return true
    }
  }
