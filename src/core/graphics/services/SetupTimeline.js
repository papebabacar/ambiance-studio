import S from 's-js'
import { dispatch, SEEK, NOTE_SELECT, NOTE_UNSELECT, TOOL_SELECTION, SONG_LOOP } from '@store'
import { hitCheck, reverseRect, clearMouseEvents } from '@utils'

export default (superclass) =>
  class extends superclass {
    static setupTimeline() {
      const { grids } = S.sample(this.state)
      const $cursorSelection = document.createElement('div')
      $cursorSelection.setAttribute('data-selection', true)
      this.$timeline.append($cursorSelection)
      for (let i = 0; i < grids; i++) {
        const span = document.createElement('span')
        span.textContent = i + 1
        this.$timeline.append(span)
      }
      this.$timeline.onmousedown = (event) => {
        const { action, snap } = S.sample(this.state)
        const pointerX = Math.max(0, event.clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
        if (event.detail === 1 && action !== TOOL_SELECTION) {
          const cursorX = pointerX - (pointerX % snap)
          dispatch(SEEK, { cursorX })
          window.onmousemove = (event) => {
            const pointerX = Math.max(0, event.clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
            const cursorX = pointerX - (pointerX % snap)
            dispatch(SEEK, { cursorX })
          }
        } else {
          this.unselectAll()
          dispatch(SONG_LOOP, { start: null, end: null })
          let pointerStart = { x: pointerX, y: 0 }
          window.onmousemove = (event) => {
            const { snap, gridY } = S.sample(this.state)
            const pointerX = Math.max(0, event.clientX - this.$editor.getBoundingClientRect().left + window.scrollX)
            const { x1: start, x2: end } = reverseRect(pointerStart, { x: pointerX, y: 0 })
            const snapStart = start - (start % snap)
            const snapEnd = end + snap / 2 - ((end + snap / 2) % snap)
            const showSelection = end - start > snap / 2
            const startLoop = showSelection ? snapStart : null
            const endLoop = showSelection ? snapEnd : null
            for (const [id, note] of Object.entries(this.activeNotes)) {
              const { x, y, width, selected: prevSelected } = note
              const selected = hitCheck(
                { x, y, width, height: gridY },
                { x: startLoop, y: 0, width: endLoop - startLoop, height: this.$editor.getBoundingClientRect().height }
              )
              this.$notes[id].setAttribute('data-selected', selected)
              if (selected !== prevSelected) {
                selected ? dispatch(NOTE_SELECT, { id }) : dispatch(NOTE_UNSELECT, { id })
              }
            }
            dispatch(SONG_LOOP, {
              start: startLoop,
              end: endLoop
            })
            const cursorX = snapStart
            dispatch(SEEK, { cursorX })
          }
        }
        window.onmouseup = () => clearMouseEvents()
      }
    }
  }
