import S from 's-js'
import { state, dispatch, PIANOROLL_VIEW, SILENCE } from '@store'
import { snapLength } from '@utils'
import { toPlaylist } from '@utils/midi'
import { PluginManager } from '@core/plugin'

export default (superclass) =>
  class extends superclass {
    static render() {
      this.activeNotes = {}
      this.selection = {}
      S(() => {
        if (this.filterByPattern) {
          const selectedPattern = state.selectedPattern()
          if (selectedPattern && !this.$notes[selectedPattern]) {
            const $notePattern = document.createElement('div')
            this.$editor.append($notePattern)
            this.$notes[selectedPattern] = $notePattern
            const $controlPattern = document.createElement('div')
            this.$controller.append($controlPattern)
            this.$controllers[selectedPattern] = $controlPattern
          }
          if (this.selectedPattern) {
            this.$notes[this.selectedPattern].attributeStyleMap.set('transform', 'translate3d(-1000%,0,0)')
            this.$notes[selectedPattern].attributeStyleMap.set('transform', 'translate3d(0,0,0)')
            this.$controllers[this.selectedPattern].attributeStyleMap.set('transform', 'translate3d(-1000%,0,0)')
            this.$controllers[selectedPattern].attributeStyleMap.set('transform', 'translate3d(0,0,0)')
          }
          this.selectedPattern = selectedPattern
          if (!S.sample(state.songMode)) setImmediate(() => dispatch(SILENCE))
          if (state.view() !== PIANOROLL_VIEW) return
          this.unselectAll()
        }
        S(() => {
          this.activeNotes = {}
          this.selection = {}
          let length = null
          let minY = null
          let maxY = null
          let isNoteAdded = false
          for (const [id, note] of Object.entries(this.notes())) {
            const { x, y, width, mute, controls, selected } = note
            isNoteAdded =
              this.addNote({
                id,
                x,
                y,
                width,
                controls,
                mute: this.filterByPattern
                  ? mute
                  : S.sample(state.mutedTracks)[Math.floor(y / S.sample(this.state).gridY)]
                  ? true
                  : mute,
                selected
              }) || isNoteAdded
            if (this.filterByPattern) {
              if (S.sample(state.patterns)[this.selectedPattern].notes[id]) {
                this.activeNotes[id] = note
                selected && (this.selection[id] = true)
                length = Math.max(length, x + width)
                minY = minY !== null ? Math.min(minY, y) : y
                maxY = Math.max(maxY, y)
                S.sample(state.patterns)[this.selectedPattern].length = length
                S.sample(state.patterns)[this.selectedPattern].minY = minY
                S.sample(state.patterns)[this.selectedPattern].maxY = maxY
              }
            } else {
              this.activeNotes[id] = note
              selected && (this.selection[id] = true)
              length = Math.max(length, x + width)
            }
          }
          Object.keys(state.added).map((id) => !this.notes()[id] && this.$notes[id] && this.deleteNote(id))
          Object.keys(state.deleted).map((id) => !this.notes()[id] && this.$notes[id] && this.deleteNote(id))
          if (
            S.sample(this.state).length !== length ||
            S.sample(this.state).minY !== minY ||
            S.sample(this.state).maxY !== maxY
          ) {
            setImmediate(() => {
              S.sample(this.state).length = length
              S.sample(this.state).minY = minY
              S.sample(this.state).maxY = maxY
            })
          }
          if (
            isNoteAdded ||
            Object.keys(state.added).length ||
            Object.keys(state.deleted).length ||
            Object.keys(state.dirty).length
          ) {
            const bar = S.sample(state.playlist).gridX
            S.sample(state.playlist).currentWidth = snapLength(toPlaylist(length), bar)
            Object.entries(S.sample(state.clips)).map(([id, clip]) => {
              if (clip.pattern === S.sample(state.selectedPattern)) {
                PluginManager.drawNote(this.$notes[id], clip)
              }
            })
            this.filterByPattern && PluginManager.drawNote(this.$minimap, { pattern: S.sample(state.selectedPattern) })
          }
          const { start, end } = this.state()
          const $songSelection = this.$highlights.querySelector('div[data-selection]')
          const $cursorSelection = this.$timeline.querySelector('div[data-selection]')
          if (start !== null && end !== null) {
            $cursorSelection.attributeStyleMap.set('visibility', 'visible')
            $cursorSelection.attributeStyleMap.set('transform', `translate3d(${start}px,0,0)`)
            $cursorSelection.attributeStyleMap.set('width', CSS.px(end - start))
            $songSelection.attributeStyleMap.set('visibility', 'visible')
            $songSelection.attributeStyleMap.set('transform', `translate3d(${start}px,0,0)`)
            $songSelection.attributeStyleMap.set('width', CSS.px(end - start))
          } else {
            $cursorSelection.attributeStyleMap.set('visibility', 'hidden')
            $songSelection.attributeStyleMap.set('visibility', 'hidden')
          }
          state.added = {}
          state.deleted = {}
          state.dirty = {}
        })
      })
    }
  }
