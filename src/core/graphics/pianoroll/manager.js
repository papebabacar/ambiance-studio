import { Manager } from '../manager'
import { mix } from '@utils'
import { state } from '@store'
import S from 's-js'
import { getPianorollSnapWidth } from '@utils/midi'
import services from './services'
// import * as Note from 'tonal-note'
import * as Scale from 'tonal-scale'
import { CONTROLS_DEFAULT } from '@constants'
import style from '@components/Main/Pianoroll/Keyboard/style'

export class PianorollManager extends mix(Manager).with(...Object.values(services)) {
  static get state() {
    return state.pianoroll
  }

  static get notes() {
    return state.notes
  }

  static get filterByPattern() {
    return true
  }

  static setSize(x) {
    const { gridX, grids } = S.sample(state.pianoroll)
    const bar = (gridX * S.sample(state.numerator) * 16) / S.sample(state.denominator)
    if (x > grids * bar - bar * 2) {
      S.sample(state.pianoroll).grids = grids + 2
      document.body.attributeStyleMap.set('--pianoroll-bars', grids)
    }
  }

  static getSnapWidth(snap) {
    return getPianorollSnapWidth(snap)
  }

  static set keyboard(keyboard) {
    this.$keyboard = keyboard
  }

  static set controller(controller) {
    this.$controller = controller
    this.setupControls()
  }

  static setupEditor() {
    this.$controllers = {}
    this.$keys = {}
    super.setupEditor()
  }

  static drawNote($note, { x, y, selected, controls }) {
    $note.setAttribute('data-key', this.getNote(y))
    const control = S.sample(state.controlView)
    this.drawControl(control, { id: $note.id, x, selected, controls })
  }

  static drawControl(control, { id, x, selected, controls }) {
    const $control = this.$controllers[id]
    const { value = 90, type = 'normal' } = CONTROLS_DEFAULT[control]
    $control.attributeStyleMap.set('transform', `translate3d(${x}px,0,0)`)
    selected ? $control.setAttribute('selected', '') : $control.removeAttribute('selected')
    if (type === 'center') {
      const height = (controls[control] !== undefined ? controls[control] : value) - 50
      if (height < 0) {
        $control.attributeStyleMap.set('height', `${Math.abs(height)}%`)
        $control.attributeStyleMap.set('bottom', `calc(50% - ${Math.abs(height)}%)`)
        $control.setAttribute('revert', '')
      } else {
        $control.attributeStyleMap.set('height', `${height}%`)
        $control.attributeStyleMap.set('bottom', '50%')
        $control.removeAttribute('revert')
      }
    } else {
      const height = controls[control] !== undefined ? controls[control] : value
      $control.attributeStyleMap.set('height', `${height}%`)
      $control.attributeStyleMap.set('bottom', 0)
      $control.removeAttribute('revert')
    }
  }

  // static getNoteDomNode(frequency) {
  //   const { step, chroma, alt, oct } = Note.props(Note.fromMidi(Note.freqToMidi(frequency)))
  //   const stepNumber = 10 * 7 - (step + 1) - 7 * oct
  //   const noteNumber = 10 * 12 - (chroma + 1) - 12 * oct
  //   if (stepNumber < 0) return {}
  //   return {
  //     noteNumber,
  //     note: this.$keyboard.querySelector(`li:nth-of-type(${stepNumber + 1}) ${alt ? 'span' : 'div'}`)
  //   }
  // }

  static getNote(y) {
    const scale = Scale.notes('C chromatic')
    const octave = 9 - Math.floor(y / (12 * S.sample(state.pianoroll).gridY))
    const note = (9 - octave) * 12 + (11 - y / S.sample(state.pianoroll).gridY)
    return scale[note] + octave
  }

  static drawNoteOn(noteNumber) {
    const $key = this.$keys[noteNumber] || this.$keyboard.querySelector(`#key${noteNumber}`)
    requestAnimationFrame(() => $key.classList.add(style.active))
    this.$keys[noteNumber] = $key
  }

  static drawNoteOff(noteNumber) {
    const $key = this.$keys[noteNumber] || this.$keyboard.querySelector(`#key${noteNumber}`)
    requestAnimationFrame(() => $key.classList.remove(style.active))
    this.$keys[noteNumber] = $key
  }
}
