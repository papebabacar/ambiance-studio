import S from 's-js'

import { state, dispatch, CONTROL_SET } from '@store'
// import { History } from '@store/history'
import { clearMouseEvents } from '@utils'
import { checkIntersection } from 'line-intersect'

export default (superclass) =>
  class extends superclass {
    static setupControls() {
      S(() => {
        const control = state.controlView()
        Object.entries(this.activeNotes).map(([id, note]) => {
          this.drawControl(control, { id, ...note })
        })
      })
      this.$controller.onmousedown = ({ offsetX, offsetY }) => {
        this.setControls({ pointerX: offsetX, pointerY: offsetY })
        window.onmousemove = ({ clientX, clientY }) => {
          const pointerX = Math.max(0, clientX - this.$controller.getBoundingClientRect().left + window.scrollX)
          const pointerY = Math.max(0, clientY - this.$controller.getBoundingClientRect().top + window.scrollY)
          this.setControls({ pointerX, pointerY })
        }
        window.onmouseup = () => {
          clearMouseEvents()
        }
      }
    }
    static setControls({ pointerX, pointerY }) {
      const getActiveNotes = () => {
        const selectedItems = Object.keys(this.selection)
        return selectedItems.length
          ? selectedItems.reduce((items, id) => {
              return { ...items, [id]: { ...this.activeNotes[id] } }
            }, {})
          : this.activeNotes
      }
      let offset = 12
      const { height } = this.$controller.getBoundingClientRect()
      const ids = Object.entries(getActiveNotes()).reduce((notes, [id, note]) => {
        const { x } = note
        const { type } = checkIntersection(x, 0, x, height, pointerX - offset, pointerY, pointerX + offset, pointerY)
        return type === 'intersecting' ? [...notes, id] : notes
      }, [])
      if (ids.length) {
        const control = S.sample(state.controlView)
        const value = Math.round(((height - pointerY) / height) * 100)
        dispatch(CONTROL_SET, { ids, [control]: value })
      }
    }
  }
