class GridPainter {
  static get inputProperties() {
    return ['--numerator', '--denominator', '--pianoroll-snap', '--pianoroll-grid-x', '--pianoroll-grid-y']
  }
  paint(ctx, geom, properties) {
    const numerator = Number(properties.get('--numerator').value)
    const denominator = Number(properties.get('--denominator').value)
    const sizeX = Number(properties.get('--pianoroll-grid-x').value)
    const sizeY = Number(properties.get('--pianoroll-grid-y').value)
    const snap = Number(properties.get('--pianoroll-snap').value)
    const grid = snap === 1 ? sizeX : snap
    const bar = (numerator * 16) / denominator
    const beginX = 0
    const beginY = 0
    ctx.strokeStyle = 'rgb(44, 44, 44)'
    ctx.beginPath()
    ctx.fillStyle = 'rgb(44, 60, 70)'
    for (let x = beginX; x < geom.width / grid; x++) {
      ctx.moveTo(x * grid, beginY)
      ctx.lineTo(x * grid, geom.height)
      if (x % denominator === 0) {
        ctx.moveTo(x * grid, beginY)
        ctx.lineTo(x * grid, geom.height)
      }
      if (x % bar === 0) {
        ctx.moveTo(x * grid, beginY)
        ctx.lineTo(x * grid, geom.height)
      }
      // Four bars overlay
      if (Math.floor(x / (4 * ((bar * sizeX) / grid))) % 2) {
        ctx.fillRect(x * grid, beginY, grid, geom.height)
      }
    }
    ctx.fillStyle = 'rgb(39, 55, 65)'
    for (let y = beginY; y < geom.height / sizeY; y++) {
      if ((y + 4) % 12 === 0 || (y + 11) % 12 === 0) {
        ctx.moveTo(beginX, y * sizeY)
        ctx.lineTo(geom.width, y * sizeY)
      }
      // Black notes
      if ((y + 2) % 12 === 0 || (y + 4) % 12 === 0 || (y + 7) % 12 === 0 || (y + 9) % 12 === 0 || (y + 11) % 12 === 0) {
        ctx.fillRect(beginX, y * sizeY, geom.width, sizeY)
      }
    }
    ctx.stroke()
  }
}

// eslint-disable-next-line no-undef
registerPaint('pianoroll', GridPainter)
