class GridPainter {
  static get inputProperties() {
    return ['--numerator', '--playlist-snap', '--playlist-grid-x', '--playlist-grid-y']
  }
  paint(ctx, geom, properties) {
    const numerator = Number(properties.get('--numerator').value)
    const sizeX = Number(properties.get('--playlist-grid-x').value)
    const sizeY = Number(properties.get('--playlist-grid-y').value)
    const snap = Number(properties.get('--playlist-snap').value)
    const grid = snap === 1 ? sizeX / numerator : snap
    const beginX = 0
    const beginY = 0
    ctx.strokeStyle = 'rgb(44, 44, 44)'
    ctx.fillStyle = 'rgb(44, 60, 70)'
    ctx.beginPath()
    for (let x = beginX; x < geom.width / grid; x++) {
      ctx.moveTo(x * grid, beginY)
      ctx.lineTo(x * grid, geom.height)
      if (x % (sizeX / grid) === 0) {
        ctx.moveTo(x * grid, beginY)
        ctx.lineTo(x * grid, geom.height)
      }
      if (x % (4 * (sizeX / grid)) === 0) {
        ctx.moveTo(x * grid, beginY)
        ctx.lineTo(x * grid, geom.height)
      }
      // Four bars overlay
      if (Math.floor(x / (4 * (sizeX / grid))) % 2) {
        ctx.fillRect(x * grid, beginY, grid, geom.height)
      }
    }
    for (let y = beginY; y < geom.height / sizeY; y++) {
      ctx.moveTo(beginX, y * sizeY)
      ctx.lineTo(geom.width, y * sizeY)
    }
    ctx.stroke()
  }
}

// eslint-disable-next-line no-undef
registerPaint('playlist', GridPainter)
