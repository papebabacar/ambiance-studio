import { Manager } from '../manager'
import { state } from '@store'
import S from 's-js'
import { getPlaylistSnapWidth } from '@utils/midi'
import { PluginManager } from '@core/plugin'

export class PlaylistManager extends Manager {
  static get state() {
    return state.playlist
  }

  static get notes() {
    return state.clips
  }

  static get leftResizable() {
    return true
  }

  set tracks($tracks) {
    this.$tracks = $tracks
  }

  static setSize(x, y) {
    const { gridX, gridY, grids, tracks } = S.sample(state.playlist)
    if (x > grids * gridX - gridX * 2) {
      S.sample(state.playlist).grids = grids + 32
      document.body.attributeStyleMap.set('--playlist-bars', grids)
    }
    if (y > tracks * gridY - gridY * 2) {
      state.playlist().tracks = tracks + 2
      // document.body.attributeStyleMap.set('--playlist-bars', grids)
    }
  }

  static getSnapWidth(snap) {
    return getPlaylistSnapWidth(snap)
  }

  static drawNote($note, clip) {
    PluginManager.drawNote($note, clip)
  }
}
