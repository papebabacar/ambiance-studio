// export const CLOCK_STEP = 0.05 // preferred step
// export const CLOCK_STEP = 0.125 // preferred step
export const CLOCK_STEP = 0.1 // preferred step
export const CLOCK_AHEAD = 0.025 // ahead of time buffer
export const CONTROLS_DEFAULT = {
  velocity: { name: 'Velocity', value: 90 },
  timbre: { name: 'Timbre', value: 70 },
  pan: { name: 'Pan', value: 50, type: 'center' },
  pitch: { name: 'Pitch', value: 50, type: 'center' }
}
