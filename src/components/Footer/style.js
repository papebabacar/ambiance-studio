import { css } from 'astroturf'

export default css`
  .footer {
    grid-column: 1 / span 23;
    grid-row: 16 / span 1;
    text-align: center;
    z-index: 999999999;
    background: #4c4c4c4c;
    background: #2a2a35;
    display: flex;
    border-top: 2px solid #555;
  }
`
