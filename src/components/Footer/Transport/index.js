import * as Surplus from 'surplus'

import Timer from './Timer'

import style from './style'

const Component = () => {
  return (
    <div class={style.transport}>
      <Timer />
    </div>
  )
}

export default Component
