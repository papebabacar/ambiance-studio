import * as Surplus from 'surplus'

import { state, dispatch, SET_NUMERATOR, SET_DENOMINATOR, SET_TEMPO } from '@store'

import { microsecondsPQN, bpm, pxInSeconds } from '@utils/midi'
import style from './style'

const Component = () => {
  return (
    <div class={style.timer}>
      <div>{showTime()}</div>
      <div>
        <input
          type="number"
          oninput={(e) => dispatch(SET_TEMPO, { microsecondsPQN: microsecondsPQN(Number(e.target.value)) })}
          min="10"
          value={bpm(state.microsecondsPQN())}
          max="522"
        />
        <span> BPM </span>
      </div>

      <div>
        <select
          onchange={(e) => dispatch(SET_NUMERATOR, { numerator: Number(e.target.value) })}
          value={state.numerator()}
        >
          {Array(16)
            .fill()
            .map((_, index) => (
              <option key={index} value={index + 1}>
                {index + 1}
              </option>
            ))}
        </select>
        <select
          onchange={(e) => dispatch(SET_DENOMINATOR, { denominator: Number(e.target.value) })}
          value={state.denominator()}
        >
          <option value="2">2</option>
          <option value="4">4</option>
          <option value="8">8</option>
          <option value="16">16</option>
          <option value="32">32</option>
        </select>
      </div>
    </div>
  )
}

export default Component

const showTime = () => {
  const songTime = pxInSeconds(state.cursorX())
  return `${String(Math.floor(songTime / 60)).padStart(2, '0')} : ${String(
    Math.floor(songTime) - Math.floor(songTime / 60) * 60
  ).padStart(2, '0')} : ${showMs()}`
  //   <span key={0}>{showMs()}</span>
  // ]
}

const showMs = () => {
  const songTime = pxInSeconds(state.cursorX())
  return `${String(Math.floor(Number(songTime - Math.floor(songTime)) * 1000)).padStart(3, '0')} `
}
