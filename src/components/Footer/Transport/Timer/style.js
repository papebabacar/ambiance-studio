import { css } from 'astroturf'

export default css`
  .timer {
    display: inline-block;
    background: #2f70c1;
    div {
      background: rgba(0, 0, 0, 0.5);
      padding: 0.203125em 0.40625em;
      font-size: 150%;
      display: inline-block;
      border: 1px solid #a7b2d626;
      width: 6.25em;
      input {
        width: 3em;
        font-size: 90%;
        background: transparent;
        border: none;
        color: var(--color);
        text-align: right;
      }
      span {
        font-size: 80%;
      }
    }
  }
`
