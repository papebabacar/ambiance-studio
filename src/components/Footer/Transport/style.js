import { css } from 'astroturf'

export default css`
  .transport {
    margin: 0 0.40625em;
    padding: 0.125em 0.40625em;
    background: rgba(40, 42, 47, 0.3);
  }
`
