import { css } from 'astroturf'

export default css`
  .playback {
    margin: 0 0.40625em;
    padding: 0.125em 0.40625em;
    background: rgba(40, 42, 47, 0.3);
    > div {
      background: #33333333;
      padding: 0.40625em 0.203125em;
      display: block;
      vertical-align: top;
      border-radius: 8px;
    }
    a {
      vertical-align: top;
      line-height: 0.40625em;
      text-shadow: 0 -2px 1px rgba(0, 0, 0, 0.5);
      font-size: 1.5em;
      text-indent: 0.1em;
      padding: 0.125em 0.3125em;
      background: #b7ece4;
      background: linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.2));
      display: inline-block;
      margin: 0.203125em 0.3125em 0;
      color: rgb(255, 255, 250);
      border-radius: 1px;
      box-shadow: inset 2px 4px 5px -3px rgba(214, 195, 195, 0.5), inset -2px -4px 5px rgba(20, 36, 70, 0.4),
        0 0 0 2px rgba(69, 69, 76, 0.14), -2px -2px 0 2px rgba(0, 0, 0, 0.2), 2px 2px 0 2px rgba(75, 75, 75, 0.3),
        0 0 0 4px rgba(75, 75, 75, 0.1), 0 0 0 5px rgba(0, 0, 0, 0.5);
      &:active {
        color: rgba(155, 125, 180, 1);
        animation: neon 0.5s ease-in-out infinite alternate;
        box-shadow: inset -2px -4px 5px -3px rgba(0, 0, 0, 0.5), inset 2px 4px 5px rgba(0, 0, 0, 0.5),
          0 0 0 2px rgba(0, 0, 0, 0.4), -2px -2px 0 2px rgba(0, 0, 0, 0.2), 2px 2px 0 2px rgba(75, 75, 75, 0.3),
          0 0 0 4px rgba(75, 75, 75, 0.1), 0 0 0 5px rgba(0, 0, 0, 0.5);
      }
    }
  }
`
