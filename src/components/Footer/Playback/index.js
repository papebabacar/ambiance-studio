import * as Surplus from 'surplus'
import { state, dispatch, PLAY, PAUSE, STOP, RECORD } from '@store'
import Cursor from './Cursor'
import style from './style'

const Component = () => {
  return (
    <div class={style.playback}>
      <Cursor />
      <div>
        <a onclick={() => dispatch(!state.playing() ? PLAY : PAUSE)}>
          <i class={`icon-${!state.playing() ? 'play' : 'pause'}`} />
        </a>
        <a onclick={() => dispatch(STOP)}>
          <i class="icon-stop" />
        </a>
        <a onclick={() => dispatch(RECORD, { recording: !state.recording() })}>
          <i class="icon-stop" />
        </a>
      </div>
    </div>
  )
}

export default Component
