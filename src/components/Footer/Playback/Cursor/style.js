import { css } from 'astroturf'

export default css`
  .input {
    &,
    &::-webkit-slider-runnable-track,
    &::-webkit-slider-thumb {
      -webkit-appearance: none;
    }
    &::-webkit-slider-runnable-track {
      position: relative;
      width: 100%;
      height: 0.5em;
      background: #888;
      border: 1px solid #000;
    }
    &::-webkit-slider-thumb {
      margin-top: -0.125em;
      border: none;
      width: 2em;
      height: 0.5em;
      border-radius: 0.5em;
      box-shadow: -0.125em 0 0.25em #928886, inset -1px 0 1px #fff;
      background: radial-gradient(#ebe1e0 10%, rgba(235, 225, 224, 0.2) 10%, rgba(235, 225, 224, 0) 72%) no-repeat 50%
          50%,
        radial-gradient(at 100% 50%, #e9dfde, #eae1de 71%, transparent 71%) no-repeat 2.5em 50%,
        linear-gradient(90deg, #e9dfde, #d0c8c6) no-repeat 100% 50%,
        radial-gradient(at 0 50%, #d0c6c5, #c6baba 71%, transparent 71%) no-repeat 0.75em 50%,
        linear-gradient(90deg, #e3d9d8, #d0c6c5) no-repeat 0 50%, linear-gradient(#cdc0c0, #fcf5ef, #fcf5ef, #cdc0c0);
      background-size: 0.825em 100%;
    }
    &:focus {
      outline: none;
    }
    &:focus::-webkit-slider-runnable-track {
      background: #666;
      border: 1px solid #111;
    }
  }
`
