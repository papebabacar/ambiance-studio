import * as Surplus from 'surplus'
import S from 's-js'
import { snapLength } from '@utils'
import { state, dispatch, SEEK } from '@store'
import style from './style'

const Component = () => {
  const getLength = () => {
    const numerator = S.sample(state.numerator)
    const denominator = S.sample(state.denominator)
    const store = state.songMode() ? state.playlist : state.pianoroll
    const { gridX, end, length } = store()
    const barX = state.songMode() ? gridX : (gridX * numerator * 16) / denominator
    return end ? end : snapLength(length, barX)
  }
  return (
    <input
      class={style.input}
      oninput={(e) => dispatch(SEEK, { cursorX: Number(e.target.value) })}
      type="range"
      step="1"
      min="0"
      value={state.cursorX()}
      max={getLength()}
    />
  )
}

export default Component
