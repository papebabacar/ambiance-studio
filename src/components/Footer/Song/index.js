import * as Surplus from 'surplus'

import { state, dispatch, SEEK, SONG_MODE } from '@store'

import style from './style'

const Component = () => {
  return (
    <div class={style.song}>
      <label>
        <input
          type="checkbox"
          checked={state.songMode()}
          onChange={(e) => {
            dispatch(SEEK, { cursorX: 0 })
            dispatch(SONG_MODE, { songMode: e.target.checked })
          }}
        />
        <span data-on="PATTERN" data-off="SONG" />
      </label>
    </div>
  )
}

export default Component
