import { css } from 'astroturf'

export default css`
  .song {
    margin-top: 0.203125em;
    a {
      font-size: 100% !important;
    }
    span {
      cursor: pointer;
    }
    label {
      position: relative;
      display: inline-block;
      height: 2em;
      width: 8em;
    }
    label input {
      display: none;
    }
    label input + span {
      position: absolute;
      top: 0;
      right: 0;
      left: 0;
      bottom: 0;
      border-radius: 2em;
      background-color: rgba(150, 200, 155, 0.65);
      box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.4);
      transition: 0.35s cubic-bezier(0.82, 0.02, 0.58, 1);
    }
    label input + span:before {
      content: '';
      position: absolute;
      top: 0;
      bottom: 0;
      right: 0;
      left: 0;
      width: 2em;
      border-radius: 50%;
      box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.5);
      background-color: #fdefe4;
      transform: scale(0.6);
      transition: 0.3s ease-out;
    }
    label input + span:after {
      content: attr(data-on);
      position: absolute;
      top: 50%;
      left: 0;
      width: 100%;
      text-align: right;
      padding: 0px 8%;
      box-sizing: border-box;
      transform: translateY(-50%);
    }
    label input:checked + span {
      background-color: #a46484;
      text-align: right;
    }
    label input:checked + span:after {
      content: attr(data-off);
    }
    label input:checked + span:before {
      left: 6.125em;
    }
    label input:checked + span:after {
      color: #fdefe4;
      left: -1em;
    }
  }
`
