import * as Surplus from 'surplus'

import Song from './Song'
import Transport from './Transport'
import Playback from './Playback'
import style from './style'

const Component = () => (
  <div class={style.footer}>
    <Song />
    <Playback />
    <Transport />
  </div>
)

export default Component
