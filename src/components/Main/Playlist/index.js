import * as Surplus from 'surplus'

import style from './style'
import commonStyle from '@components/Common/Arrangement'
import { ui } from '@components/Common'
import Tracks from './Tracks'
import { PlaylistManager } from '@core/graphics/playlist'
import {
  state,
  dispatch,
  TOOL_POINTER,
  TOOL_PAINTER,
  TOOL_SELECTION,
  TOOL_DELETE,
  TOOL_MUTE,
  TOOL_CUT,
  SET_PLAYLIST_SNAP,
  SET_PLAYLIST_GRID_X,
  SET_PLAYLIST_GRID_Y
} from '@store'

let $container
let $editor
let $highlights
let $timeline
let $cursor
let $tracks

const Component = () => [
  <div class={commonStyle.nav} key={0}>
    {' '}
    <a onclick={() => dispatch(TOOL_POINTER)}>Pointer</a> <a onclick={() => dispatch(TOOL_PAINTER)}>Painter</a>{' '}
    <a onclick={() => dispatch(TOOL_SELECTION)}>Selection</a> <a onclick={() => dispatch(TOOL_DELETE)}>Delete</a>{' '}
    <a onclick={() => dispatch(TOOL_MUTE)}>Mute</a> <a onclick={() => dispatch(TOOL_CUT)}>Cut</a>{' '}
    <select value={state.playlist().snapType} onchange={e => dispatch(SET_PLAYLIST_SNAP, { snap: e.target.value })}>
      <option value="BAR">Bar</option>
      <option value="BEAT">Beat</option>
      <option value="1/2BEAT">1/2 Beat</option>
      <option value="1/3BEAT">1/3 Beat</option>
      <option value="1/4BEAT">1/4 Beat</option>
      <option value="1/6BEAT">1/6 Beat</option>
      <option value="1/12BEAT">1/12 Beat</option>
      <option value="1/16BEAT">1/16 Beat</option>
      <option value="NONE">None</option>
    </select>
    <input
      type="number"
      oninput={e => dispatch(SET_PLAYLIST_GRID_X, { grid: Number(e.target.value) })}
      step={state.numerator()}
      min={state.numerator() * 6}
      value={state.playlist().gridX}
      max={state.numerator() * 1200}
    />
    <input
      type="number"
      oninput={e => dispatch(SET_PLAYLIST_GRID_Y, { grid: Number(e.target.value) })}
      step="4"
      min="20"
      value={state.playlist().gridY}
      max="260"
    />
  </div>,
  <div
    class={`${commonStyle.screen} ${style.playlist}`}
    key={1}
    fn={() => {
      state.songMode() ? $cursor.classList.add(commonStyle.visible) : $cursor.classList.remove(commonStyle.visible)
      state.playing() ? $cursor.classList.add(commonStyle.playing) : $cursor.classList.remove(commonStyle.playing)
    }}
  >
    <div
      class={`${ui.scrollbar} ${commonStyle.container}`}
      ref={$container}
      fn={() => {
        PlaylistManager.container = $container
      }}
    >
      <div class={commonStyle.timeline} ref={$timeline} fn={() => (PlaylistManager.timeline = $timeline)}>
        <div
          class={commonStyle.cursor}
          ref={$cursor}
          fn={() => {
            const cursorX = state.cursorX()
            requestAnimationFrame(() => $cursor.attributeStyleMap.set('transform', `translate3d(${cursorX}px, 0, 0)`))
          }}
        />
      </div>
      <Tracks
        ref={$tracks}
        fn={() => {
          PlaylistManager.tracks = $tracks
        }}
      />
      <div class={commonStyle.canvas}>
        <div class={commonStyle.highlights} ref={$highlights} fn={() => (PlaylistManager.highlights = $highlights)} />
        <div
          class={`${commonStyle.editor} ${style.editor}`}
          ref={$editor}
          fn={() => {
            PlaylistManager.editor = $editor
          }}
        />
      </div>
    </div>
  </div>
]

export default Component
