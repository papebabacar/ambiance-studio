import { css } from 'astroturf'

export default css`
  .tracks {
    font-size: 90%;
    z-index: 999999999;
    position: sticky;
    left: 0;
    width: 6.125em;
    height: 100%;
    background: #111;
    > ul {
      position: absolute;
      width: 100%;
      top: 0;
      background: var(--bg);
      a {
        position: relative;
        display: block;
        color: #888;
        background: #44484e;
        border: 1px solid #333;
        border-top-right-radius: 2px;
        border-bottom-right-radius: 2px;
        padding: 0 0.40625em 0 0;
        height: calc(var(--playlist-grid-y) * 1px);
        span {
          position: absolute;
          top: calc(100% - 1.40825em);
          right: 0.8125em;
          display: block;
          width: 1.25em;
          height: 1.25em;
          border: 2px solid #666;
          border-radius: 50%;
          padding: 0.203125em;
          i {
            background: greenyellow;
            border: 1px solid transparent;
            border-radius: 50%;
            display: block;
            width: 0.5em;
            height: 0.5em;
          }
        }
        &::after {
          content: '';
          background: #222;
          position: absolute;
          top: 0.40625em;
          right: 0.125em;
          display: block;
          height: calc(100% - 0.8125em);
          width: 0.40625em;
        }
        &[data-playing='true'] {
          &::after {
            background: #fcfcfc8f;
          }
        }
        &[data-hit='true'] {
          &::after {
            background: #ffffecef;
          }
        }
        &[data-mute='true'] {
          span i {
            background: #aaa;
          }
        }
      }
    }
  }
`
