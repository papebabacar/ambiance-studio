import * as Surplus from 'surplus'

import style from './style'
import { state, dispatch, TRACK_MUTE, TRACK_UNMUTE } from '@store'
import S from 's-js'

let $tracks

const Component = () => (
  <div class={style.tracks} ref={$tracks}>
    <ul>
      {() => {
        S(() => {
          let playingTracks = {}
          let hittingTracks = {}
          Object.values(state.playingNotes()).map(({ track: id, on, hit }) => {
            playingTracks[id] = on !== null || playingTracks[id]
            hittingTracks[id] = hit || hittingTracks[id]
          })
          Object.keys(playingTracks).map(id => {
            const $track = $tracks.querySelector(`[data-id='${id}']`)
            $track && $track.setAttribute('data-playing', playingTracks[id])
            $track && $track.setAttribute('data-hit', hittingTracks[id])
          })
        })
        return Array(state.playlist().tracks)
          .fill()
          .map((_, trackIndex) => (
            <li key={trackIndex}>
              <a
                data-id={trackIndex}
                data-mute={state.mutedTracks()[trackIndex]}
                onclick={() => {
                  state.mutedTracks()[trackIndex]
                    ? dispatch(TRACK_UNMUTE, { trackIndex })
                    : dispatch(TRACK_MUTE, { trackIndex })
                }}
              >
                Track {trackIndex + 1}
                <span>
                  <i />
                </span>
              </a>
            </li>
          ))
      }}
    </ul>
  </div>
)

export default Component
