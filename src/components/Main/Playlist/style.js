import { css } from 'astroturf'

export default css`
  .playlist {
    --editor-grid-x: var(--playlist-grid-x);
    --editor-grid-y: var(--playlist-grid-y);
    --editor-bars: var(--playlist-bars);
    --editor-height: calc(var(--editor-grid-y) * var(--playlist-tracks));
    --background: paint(playlist);
    content-visibility: auto;
  }
  .editor {
    div[data-selection] {
      z-index: 2;
      display: block;
      position: absolute;
      border: 3px solid gold;
      background: #67454534;
    }
    div[data-selected='true'] {
      background: rgb(210, 160, 160);
    }
    div[data-mute='true'] {
      background: #a8b2bf;
    }
  }
  .note {
    z-index: 0;
    touch-action: none;
    border: 1px solid #444;
    border-radius: 3px;
    background: #21252a5c;
    position: absolute;
    height: calc(var(--editor-grid-y) * 1px);
    &::before {
      content: 'Pattern';
      display: block;
      height: 18px;
      width: 100%;
      background: #444;
      padding: 0 0.6125em;
    }
    canvas {
      position: absolute;
      top: 18px;
    }
  }
`
