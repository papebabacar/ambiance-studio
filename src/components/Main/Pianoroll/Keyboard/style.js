import { css } from 'astroturf'

export default css`
  .keyboard {
    z-index: 1;
    position: sticky;
    left: 0;
    width: 6.125em;
    height: 100%;
    background: #fff;
    > div {
      position: absolute;
      width: 100%;
      top: calc(var(--pianoroll-grid-y) * -1px);
      height: calc(var(--pianoroll-grid-y) * 1px);
      background: var(--bg);
      z-index: 999;
    }
    ul {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      counter-reset: note 9;
      cursor: pointer;
    }

    ul li {
      position: relative;
    }

    ul li div {
      display: block;
      height: calc(var(--pianoroll-grid-y) * 2px);
      background: linear-gradient(-30deg, #f5f5f5, #fff);
      box-shadow: inset 0 1px 0px #fff, inset 0 -1px 0px #fff, inset 1px 0px 0px #fff, inset -1px 0px 0px #fff,
        0 4px 3px rgba(20, 20, 20, 0.7);
    }

    /* Black Tuts */
    ul li span {
      position: absolute;
      top: calc(var(--pianoroll-grid-y) * 3 / 2 * 1px - 1px);
      width: 56.67%;
      height: calc(var(--pianoroll-grid-y) * 1px + 2px);
      background: linear-gradient(-20deg, #333, #222, #333);
      z-index: 10;
      border-style: solid;
      border-color: #666 #222 #111 #555;
      box-shadow: inset 0px -1px 2px rgba(255, 255, 255, 0.4), 0 2px 3px rgba(20, 20, 20, 0.4);
      border-radius: 0 0 2px 2px;
    }

    ul li span[data-active] {
      box-shadow: inset 0px -1px 1px rgba(255, 255, 255, 0.4);
      border-width: 1px 2px 1px;
    }

    /* size adjustements */
    ul li[data-fit] div {
      height: calc(var(--pianoroll-grid-y) * 3 / 2 * 1px);
    }

    ul li[data-fit] span {
      top: calc(var(--pianoroll-grid-y) * 1px - 1px);
    }

    ul li[data-key='E'] div {
      border-top: 1px solid #ccc;
    }

    ul li[data-key='C'] div {
      background: #ccc;
    }

    ul li[data-key='C'] + li {
      background: #ccc;
      counter-increment: note -1;
    }

    /* notes */
    ul li div:after {
      color: #444;
      position: absolute;
      right: 0.40625em;
      font-weight: 900;
      font-size: calc(0.8em + (1.5 - 0.8) * ((var(--pianoroll-grid-y) * 1em - 15em) / 50));
      top: calc(20% + (45 - 20) * ((var(--pianoroll-grid-y) * 1% - 15%) / 50));
    }
    ul li:nth-of-type(7n + 7) div:after {
      content: 'C' counter(note);
    }
    ul li:nth-of-type(7n + 6) div:after {
      content: 'D' counter(note);
    }
    ul li:nth-of-type(7n + 5) div:after {
      content: 'E' counter(note);
      top: calc(2% + (20 - 2) * ((var(--pianoroll-grid-y) * 1% - 15%) / 50));
    }
    ul li:nth-of-type(7n + 4) div:after {
      content: 'F' counter(note);
    }
    ul li:nth-of-type(7n + 3) div:after {
      content: 'G' counter(note);
    }
    ul li:nth-of-type(7n + 2) div:after {
      content: 'A' counter(note);
    }
    ul li:nth-of-type(7n + 1) div:after {
      content: 'B' counter(note);
      top: calc(2% + (20 - 2) * ((var(--pianoroll-grid-y) * 1% - 15%) / 50));
    }
  }
  /* highlight */

  .active {
    background: #ffb13f !important;
    /* &:before {
      content: '';
      height: 100%;
      background: #ffb13faf;
      display: block;
    } */
  }
`
