import * as Surplus from 'surplus'

import { dispatch, NOTE_ON, NOTE_OFF } from '@store'
import style from './style'
import { PianorollManager } from '@core/graphics/pianoroll'
import { clearMouseEvents } from '@utils'

let $keyboard
let playing
let hovering
let currentNote

const Component = () => (
  <div class={style.keyboard}>
    <div />
    <ul
      id="piano-marker"
      ref={$keyboard}
      onmousedown={play}
      onmouseenter={() => {
        hovering = true
      }}
      onmouseleave={() => {
        hovering = false
        playing && stopNote(currentNote)
      }}
    >
      {() => {
        PianorollManager.keyboard = $keyboard
        return Array(10)
          .fill()
          .map((_, index) => [
            <li data-fit key="B">
              <div id={`key${12 * index}`} />
              <span id={`key${1 + 12 * index}`} />
            </li>,
            <li key="A">
              <div id={`key${2 + 12 * index}`} />
              <span id={`key${3 + 12 * index}`} />
            </li>,
            <li key="G">
              <div id={`key${4 + 12 * index}`} />
              <span id={`key${5 + 12 * index}`} />
            </li>,
            <li data-fit key="F">
              <div id={`key${6 + 12 * index}`} />
            </li>,
            <li data-fit data-key="E" key="E">
              <div id={`key${7 + 12 * index}`} />
              <span id={`key${8 + 12 * index}`} />
            </li>,
            <li key="D">
              <div id={`key${9 + 12 * index}`} />
              <span id={`key${10 + 12 * index}`} />
            </li>,
            <li data-fit data-key="C" key="C">
              <div id={`key${11 + 12 * index}`} />
            </li>
          ])
      }}
    </ul>
  </div>
)

const playNote = noteNumber => {
  dispatch(NOTE_ON, { noteNumber })
  currentNote = noteNumber
}

const stopNote = noteNumber => {
  dispatch(NOTE_OFF, { noteNumber })
}

const play = ({ target }) => {
  playing = true
  playNote(target.id.substring(3))
  window.onmousemove = ({ target }) => {
    const noteNumber = target.id.substring(3)
    if (hovering && currentNote !== noteNumber) {
      stopNote(currentNote)
      playNote(noteNumber)
    }
  }
  window.onmouseup = () => {
    playing = false
    clearMouseEvents()
    stopNote(currentNote)
  }
}

export default Component
