import { css } from 'astroturf'

export default css`
  .pianoroll {
    --editor-grid-x: calc(var(--pianoroll-grid-x) * var(--numerator) * 16 / var(--denominator));
    --editor-grid-y: var(--pianoroll-grid-y);
    --editor-bars: var(--pianoroll-bars);
    --editor-height: calc(var(--pianoroll-grid-y) * 120);
    --background: paint(pianoroll);
    content-visibility: auto;
  }
  .editor {
    div[data-selection] {
      z-index: 2;
      display: block;
      position: absolute;
      border: 3px solid gold;
      background: #67454534;
    }
    div[data-selected='true'] {
      background: rgb(210, 160, 160);
    }
    div[data-mute='true'] {
      background: #a8b2bf;
    }
  }
  .parameters {
    z-index: 1;
    width: calc(var(--editor-grid-x) * var(--editor-bars) * 1px + 6.125em);
    position: sticky;
    top: calc(100% - 10.6125em);
    left: 0;
    height: 10.6125em;
    margin-top: -12.8125em;
    contain: strict;
  }
  .canvas {
    margin-left: 6.125em;
    position: absolute;
    top: 0.6125em;
    width: 100%;
    height: calc(100% - 0.6125em);
    background: rgb(48, 64, 74);
    background-image: paint(parameters);
    div {
      position: absolute;
      width: 100%;
      height: 100%;
    }
    span {
      width: 0.2em;
      pointer-events: none;
      background: #88d2af;
      position: absolute;
      height: 75%;
      left: 0;
      bottom: 0;
      &[selected] {
        background: rgb(210, 160, 160);
        z-index: 2;
      }
      &::before {
        content: '';
        display: block;
        width: 0.5em;
        height: 2px;
        background: inherit;
        position: absolute;
      }
      &[revert] {
        &::before {
          bottom: 0;
        }
      }
    }
  }
  .controls {
    display: block;
    width: 6.125em;
    height: 100%;
    background: #545758;
    position: sticky;
    left: 0;
    z-index: 3;
  }
  .separator {
    display: block;
    width: 100%;
    height: 0.6125em;
    background: #545758;
  }
  .note {
    touch-action: none;
    border: 1px solid #444;
    border-radius: 3px;
    background: #88d2af;
    position: absolute;
    height: calc(var(--pianoroll-grid-y) * 1px);
    &::before {
      content: attr(data-key);
      display: block;
      color: #444;
      font-size: 80%;
      position: relative;
      top: -0.203125em;
      left: 0.203125em;
    }
  }
`
