import * as Surplus from 'surplus'

import style from './style'
import commonStyle from '@components/Common/Arrangement'
import { ui } from '@components/Common'
import Keyboard from './Keyboard'
import { PianorollManager } from '@core/graphics/pianoroll'
import {
  state,
  dispatch,
  TOOL_POINTER,
  TOOL_PAINTER,
  TOOL_SELECTION,
  TOOL_DELETE,
  TOOL_MUTE,
  TOOL_CUT,
  SET_PIANOROLL_SNAP,
  SET_PIANOROLL_GRID_X,
  SET_PIANOROLL_GRID_Y,
  CONTROL_VIEW
} from '@store'
import { CONTROLS_DEFAULT } from '@constants'

let $container
let $editor
let $controller
let $highlights
let $timeline
let $cursor

const Component = () => [
  <div class={commonStyle.nav} key={0}>
    {' '}
    <a onclick={() => dispatch(TOOL_POINTER)}>Pointer</a> <a onclick={() => dispatch(TOOL_PAINTER)}>Painter</a>{' '}
    <a onclick={() => dispatch(TOOL_SELECTION)}>Selection</a> <a onclick={() => dispatch(TOOL_DELETE)}>Delete</a>{' '}
    <a onclick={() => dispatch(TOOL_MUTE)}>Mute</a> <a onclick={() => dispatch(TOOL_CUT)}>Cut</a>{' '}
    <select value={state.pianoroll().snapType} onchange={(e) => dispatch(SET_PIANOROLL_SNAP, { snap: e.target.value })}>
      <option value="BAR">Bar</option>
      <option value="BEAT">Beat</option>
      <option value="1/2BEAT">1/2 Beat</option>
      <option value="1/3BEAT">1/3 Beat</option>
      <option value="1/4BEAT">1/4 Beat</option>
      <option value="1/6BEAT">1/6 Beat</option>
      <option value="1/12BEAT">1/12 Beat</option>
      <option value="1/16BEAT">1/16 Beat</option>
      <option value="NONE">None</option>
    </select>
    <input
      type="number"
      oninput={(e) => dispatch(SET_PIANOROLL_GRID_X, { grid: Number(e.target.value) })}
      step="3"
      min="6"
      value={state.pianoroll().gridX}
      max="360"
    />
    <input
      type="number"
      oninput={(e) => dispatch(SET_PIANOROLL_GRID_Y, { grid: Number(e.target.value) })}
      step="4"
      min="8"
      value={state.pianoroll().gridY}
      max="80"
    />
  </div>,
  <div
    class={`${commonStyle.screen} ${style.pianoroll}`}
    key={1}
    fn={() => {
      !state.songMode() || (state.playing() && state.patternCursorX() !== null)
        ? $cursor.classList.add(commonStyle.visible)
        : $cursor.classList.remove(commonStyle.visible)
      state.playing() ? $cursor.classList.add(commonStyle.playing) : $cursor.classList.remove(commonStyle.playing)
    }}
  >
    <div
      class={`${ui.scrollbar} ${commonStyle.container}`}
      ref={$container}
      fn={() => {
        PianorollManager.container = $container
      }}
    >
      <div class={commonStyle.timeline} ref={$timeline} fn={() => (PianorollManager.timeline = $timeline)}>
        <div
          class={commonStyle.cursor}
          ref={$cursor}
          fn={() => {
            const cursorX = state.cursorX()
            const songMode = state.songMode()
            const patternCursorX = state.patternCursorX()
            requestAnimationFrame(() =>
              $cursor.attributeStyleMap.set(
                'transform',
                `translate3d(${songMode ? patternCursorX || 0 : cursorX}px, 0, 0)`
              )
            )
          }}
        />
      </div>
      <Keyboard />
      <div class={commonStyle.canvas}>
        <div class={commonStyle.highlights} ref={$highlights} fn={() => (PianorollManager.highlights = $highlights)} />
        <div
          class={`${commonStyle.editor} ${style.editor}`}
          ref={$editor}
          fn={() => {
            PianorollManager.editor = $editor
          }}
        />
      </div>
      <div class={style.parameters}>
        <span class={style.separator} />
        <div class={style.controls} onchange={({ target }) => dispatch(CONTROL_VIEW, { view: target.value })}>
          <select value={state.controlView()}>
            {Object.entries(CONTROLS_DEFAULT).map(([control, { name }]) => (
              <option key={control} value={control}>
                {name}
              </option>
            ))}
          </select>
        </div>
        <div
          class={style.canvas}
          ref={$controller}
          fn={() => {
            PianorollManager.controller = $controller
          }}
        ></div>
      </div>
    </div>
  </div>
]

export default Component
