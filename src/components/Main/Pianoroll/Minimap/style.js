import { css } from 'astroturf'

export default css`
  .minimap {
    display: inline-block;
    width: 400px;
    height: 100%;
    background: #222222;
    background-size: contain;
    background-repeat: no-repeat;
  }
`
