import * as Surplus from 'surplus'

import style from './style'

const Component = () => (
  <div class={style.minimap}>
    <canvas />
  </div>
)

export default Component
