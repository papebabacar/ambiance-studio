import { css } from 'astroturf'

export default css`
  .generators {
    border: 3px solid #555;
    z-index: 0;
    position: absolute;
    top: 0;
    left: 0;
    width: 13.8em;
    height: 100%;
    background: #2a2a35;
    overflow-y: auto;
    a[data-button] {
      display: block;
      width: 100%;
      padding: 0.40625em;
      margin-bottom: 1.625em;
      text-align: center;
      font-weight: 900;
      border: 1px solid #666;
      height: 3.25em;
      border-radius: 3px;
    }

    a[data-generator] {
      position: relative;
      display: block;
      color: #ccdfff;
      background: #44484e;
      border: 2px solid #777;
      border-top-right-radius: 2px;
      border-bottom-right-radius: 2px;
      padding: 0.40625em;
      margin-bottom: 0.203125em;
      height: 3.8125em;
      span {
        position: absolute;
        bottom: 0.40625em;
        right: 0.8125em;
        display: block;
        width: 1.25em;
        height: 1.25em;
        border: 2px solid #666;
        border-radius: 50%;
        padding: 0.203125em;
        i {
          background: greenyellow;
          border: 1px solid transparent;
          border-radius: 50%;
          display: block;
          width: 0.5em;
          height: 0.5em;
        }
      }
      &::after {
        content: '';
        background: #4c5452;
        position: absolute;
        top: 0.40625em;
        right: 0.203125em;
        display: block;
        height: calc(100% - 0.8125em);
        width: 0.203125em;
      }
      &[data-playing='true'] {
        &::after {
          background: #fcfcfc8f;
        }
      }
      &[data-hit='true'] {
        &::after {
          background: #ffffecef;
        }
      }
      &[data-mute='true'] {
        color: #888;
        span i {
          background: #aaa;
        }
      }
      &[data-selected='true'] {
        border: 2px solid #ccc;
      }
    }
  }
`
