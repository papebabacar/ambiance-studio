import * as Surplus from 'surplus'
import { state, dispatch, GENERATOR_ADD, GENERATOR_MUTE, GENERATOR_SELECT } from '@store'
import { History } from '@store/history'
import S from 's-js'
import style from './style'
import { ui } from '@components/Common'

let $container

const Component = () => (
  <div class={`${ui.scrollbar} ${style.generators}`} ref={$container}>
    <a data-button onclick={addGenerator}>
      + Add..
    </a>
    <ul>{() => drawGenerators($container)}</ul>
  </div>
)

export default Component

const addGenerator = () => {
  History.remember(state)
  dispatch(GENERATOR_ADD, { name: 'Sampler', type: 'MIDI' })
}

const drawGenerators = ($container) => {
  S(() => {
    let playingGenerators = {}
    let hittingGenerators = {}
    Object.values(state.playingNotes()).map(({ generator: id, on, hit }) => {
      playingGenerators[id] = on !== null || playingGenerators[id]
      hittingGenerators[id] = hit || hittingGenerators[id]
    })
    Object.keys(playingGenerators).map((id) => {
      // const $generator = $container.querySelector(`[data-id='${id}']`)
      // $generator.setAttribute('data-playing', playingGenerators[id])
      // $generator.setAttribute('data-hit', hittingGenerators[id])
    })
  })
  const generators = state.generators()
  // if (!Object.keys(generators).length) {
  //   setImmediate(() => dispatch(GENERATOR_ADD, { name: 'Sampler', type: 'MIDI' }))
  // }
  return Object.entries(generators).map(([id, { name }]) => {
    return (
      <li key={id}>
        <a
          data-id={id}
          data-generator
          data-mute={generators[id].mute}
          data-selected={state.selectedGenerator() === id}
          onclick={() => {
            dispatch(GENERATOR_SELECT, { id })
          }}
        >
          {name}
          <span
            onclick={(event) => {
              event.stopPropagation()
              dispatch(GENERATOR_MUTE, { id, mute: !generators[id].mute })
            }}
          >
            <i />
          </span>
        </a>
      </li>
    )
  })
}
