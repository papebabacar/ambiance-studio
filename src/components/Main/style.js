import { css } from 'astroturf'

export default css`
  .main {
    grid-column: 1 / span 19;
    grid-row: 2 / 16;
    position: relative;
  }
  .panel {
    display: flex;
    margin-left: 14em;
    height: 100%;
    flex-direction: column;
    position: relative;
    top: 0;
    left: 0;
    border: 3px solid #555;
  }
`
