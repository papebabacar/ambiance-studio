import { css } from 'astroturf'

export const styled = css`
  position: relative;
  width: 9.75em;
  padding-bottom: 3.25em;
  margin-top: calc(var(--pianoroll-grid-y) * 1px);
  overflow-x: hidden;
  overflow-y: scroll;
`

export const synth = css`
  height: 4.125em;
  background: #5ca2e085;
  border: 1px solid #4c4c4c4c;
  border-radius: 2px;
  margin: 0 0.203125em 0.203125em 0.203125em;
  cursor: pointer;
  div {
    padding: 0.40625em 0.203125em;
  }
`

export const header = css`
  background: #6c6c6c6c;
  position: relative;
  font-size: 60%;
  padding: 0 0.40625em;
  font-weight: 900;
  h1 {
    line-height: 1em;
    word-break: break-all;
    font-family: monospace;
  }
`

export const controls = css`
  display: inline-flex;
  position: absolute;
  bottom: -3.203125em;
  right: -0.40625em;
`
export const button = css`
  padding: 0.203125em 0.8125em;
  text-align: center;
  background: #49bbc7ee;
  color: #eee;
  margin-right: 0.40625em;
  margin-left: auto;
`

export const active = css`
  background: #0e85dc85;
  border: 1px solid #44ee44;
`
