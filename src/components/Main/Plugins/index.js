import * as Surplus from 'surplus'

import { active, button, controls, header, styled, synth } from './style'

import { showLayer } from '@core/graphics'
import { state } from '@store'

const Component = () => (
  <div class={`${styled}`}>
    {Object.values(state.plugins()).map(({ id, name, selected }) => (
      <div key={name} onclick={select(id)} class={`${synth} ${selected ? active : ''}`}>
        <div class={header}>
          <h1>{name}</h1>
          <div class={controls}>
            <a class={button}>x</a>
          </div>
        </div>
      </div>
    ))}
  </div>
)

const select = id => () => {
  for (const plugin of Object.values(state.plugins())) {
    const selected = plugin.id === id
    // state.set('plugins', { [plugin.id]: { ...plugin, selected } })
    document.querySelector(`#${plugin.id}`).attributeStyleMap.set('display', selected ? 'block' : 'none')
  }
  showLayer(id)
}

export default Component
