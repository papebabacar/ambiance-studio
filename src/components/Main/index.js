import * as Surplus from 'surplus'

import Pianoroll from './Pianoroll'
import Playlist from './Playlist'
import style from './style'
import { state, PIANOROLL_VIEW, PLAYLIST_VIEW } from '@store'
import Generators from './Generators'

let $pattern
let $playlist

const Component = () => (
  <main class={style.main}>
    <Generators />
    <section
      class={style.panel}
      ref={$pattern}
      fn={() => $pattern.attributeStyleMap.set('display', state.view() === PIANOROLL_VIEW ? 'block' : 'none')}
    >
      <Pianoroll />
    </section>
    <section
      class={style.panel}
      ref={$playlist}
      fn={() => $playlist.attributeStyleMap.set('display', state.view() === PLAYLIST_VIEW ? 'block' : 'none')}
    >
      <Playlist />
    </section>
  </main>
)

export default Component
