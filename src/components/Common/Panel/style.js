import { css } from 'astroturf'

export default css`
  .panel {
    min-width: 6em;
    border: 1px solid #888;
    background: #0c0c0c;
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    z-index: 99999;
  }

  .panelHeader {
    position: relative;
    height: 1.8125em;
    background: #222;
    a {
      position: absolute;
      right: 0;
      padding: 0 0.203125em;
      border: 1px solid #444;
    }
  }
`
