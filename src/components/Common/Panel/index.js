import * as Surplus from 'surplus'

import style from './style'

// import interact from 'interactjs'

export default ({ id, children }) => {
  let panel
  let panelHeader
  return (
    <div
      class={style.panel}
      id={id}
      ref={panel}
      fn={() => {
        // interact(panel)
        //   .draggable({ allowFrom: panelHeader })
        //   .on('dragmove', event => {
        //     event.stopPropagation()
        //     const x = (parseFloat(panel.dataset.x) || 0) + event.dx
        //     const y = Math.max(0, Math.min(window.innerHeight - 20, (parseFloat(panel.dataset.y) || 0) + event.dy))
        //     panel.attributeStyleMap.set('transform', `translate3d(${x}px,${y}px,0)`)
        //     panel.setAttribute('data-x', x)
        //     panel.setAttribute('data-y', y)
        //   })
      }}
      data-x={50}
      data-y={50}
      style={{ transform: `translate3d(50px,50px,0)` }}
    >
      <div class={style.panelHeader} ref={panelHeader}>
        <a onclick={() => panel.attributeStyleMap.set('display', 'none')}>x</a>
      </div>
      {children}
    </div>
  )
}
