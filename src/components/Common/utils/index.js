import { css } from 'astroturf'

export const ui = css`
  .scrollbar {
    &::-webkit-scrollbar {
      width: 0.8125em;
      height: 0.8125em;
    }
    &::-webkit-scrollbar-track {
      background: #2c2c2cee;
      border: 0.125em solid #222;
    }

    &::-webkit-scrollbar-corner {
      background: #1f1f1fee;
    }

    &::-webkit-scrollbar-thumb {
      border-radius: 15%;
      background: #8888882f;
    }
    &:hover::-webkit-scrollbar-thumb,
    &:active::-webkit-scrollbar-thumb,
    &:focus::-webkit-scrollbar-thumb {
      background: #888888;
      border: 0.125em solid #777;
    }
    &:hover::-webkit-scrollbar-track,
    &:active::-webkit-scrollbar-track,
    &:focus::-webkit-scrollbar-track {
      background: #1c1c1c;
    }
  }
`
