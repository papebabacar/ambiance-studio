import * as Surplus from 'surplus'

import style from './style'

let grid

const Component = ({ items, gap, children }) => (
  <div
    class={style.grid}
    ref={grid}
    style={{ background: 'red' }}
    fn={() => {
      items && grid.attributeStyleMap.set('--items-per-line', items)
      gap && grid.attributeStyleMap.set('--grid-gap', CSS.em(gap))
    }}
  >
    {children}
  </div>
)

export default Component
