import { css } from 'astroturf'

export default css`
  .grid {
    --items-per-line: 1;
    --grid-gap: 1.625em;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(calc(100% / var(--items-per-line) - var(--grid-gap)), 1fr));
    grid-gap: var(--grid-gap);
    margin: initial;
    > * {
      max-width: none;
      width: auto;
      margin: 0;
    }
  }
`
