import { css } from 'astroturf'

export default css`
  .screen {
    display: flex;
    width: 100%;
    height: calc(100% - 3.25em);
  }
  .container {
    width: 100%;
    overflow: scroll;
    position: relative;
  }
  .canvas {
    width: calc(var(--editor-grid-x) * var(--editor-bars) * 1px);
    position: absolute;
    top: 1.625em;
    left: 6.125em;
    height: calc(var(--editor-height) * 1px);
    background: rgb(48, 64, 74);
    background-image: var(--background);
    contain: strict;
  }
  .highlights {
    z-index: -1;
    width: calc(var(--editor-grid-x) * var(--editor-bars) * 1px);
    position: absolute;
    top: 0;
    left: 0;
    height: calc(var(--editor-height) * 1px);
    div {
      width: 100%;
      height: calc(var(--editor-grid-y) * 1px);
      position: absolute;
      background: rgba(60, 85, 95, 0.35);
    }
    div[data-selection] {
      width: auto;
      background: rgba(180, 79, 83, 0.1);
      top: 0;
      height: 100%;
    }
  }
  .editor {
    width: calc(var(--editor-grid-x) * var(--editor-bars) * 1px);
    position: absolute;
    top: 0;
    left: 0;
    height: calc(var(--editor-height) * 1px);
  }
  .timeline {
    cursor: pointer;
    width: calc(var(--editor-grid-x) * var(--editor-bars) * 1px);
    position: sticky;
    top: 0;
    left: 6.125em;
    z-index: 99;
    height: 1.625em;
    background: rgb(24, 27, 30);
    color: #ccc;
    div[data-selection] {
      z-index: -1;
      position: absolute;
      background: rgb(180, 79, 83);
      top: 0;
      height: 100%;
    }
    span {
      z-index: 1;
      font-size: 80%;
      display: inline-block;
      width: calc(var(--editor-grid-x) * 1px);
    }
  }
  .cursor {
    /* will-change: transform; */
    width: 14px;
    position: absolute;
    top: 0;
    left: -7px;
    z-index: 9;
    height: 12px;
    background: rgb(222, 200, 36);
    border-bottom-left-radius: 225%;
    border-bottom-right-radius: 225%;
    pointer-events: none;
    display: none;
    &::after {
      content: '';
      position: relative;
      display: block;
      height: 100vh;
      top: 16px;
      left: 6px;
      width: 1px;
      background: rgb(140, 147, 157);
    }
  }
  .nav {
    background: #4c4c4c4c;
    padding: 0.40625em;
    height: 3.25em;
    width: 100%;
  }
  .visible {
    display: block;
  }
  .playing::after {
    background: rgb(222, 200, 36);
    /* box-shadow: -1px 2px 13px 4px rgb(182, 120, 36, 0.6); */
  }
`
