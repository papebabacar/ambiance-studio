import * as Surplus from 'surplus'

import Menu from './Menu'
import Navigation from './Navigation'
import style from './style'

const Component = () => (
  <header className={style.header}>
    <Menu />
    <Navigation />
  </header>
)

export default Component
