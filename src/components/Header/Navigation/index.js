import * as Surplus from 'surplus'

import { dispatch, UNDO, REDO, PIANOROLL_VIEW, PLAYLIST_VIEW } from '@store'

import style from './style'
import { clearMouseEvents } from '@utils'

const Component = () => {
  return (
    <div class={style.navigation}>
      <a
        onclick={() => {
          dispatch(PIANOROLL_VIEW)
          clearMouseEvents()
        }}
      >
        Track
      </a>
      <a
        onclick={() => {
          dispatch(PLAYLIST_VIEW)
          clearMouseEvents()
        }}
      >
        Playlist
      </a>
      <a>Mixer</a>
      <a onclick={() => dispatch(UNDO)}>UNDO</a>
      <a onclick={() => dispatch(REDO)}>REDO</a>
    </div>
  )
}

export default Component
