import { css } from 'astroturf'

export default css`
  .navigation {
    margin-top: 0.40625em;
    margin-left: auto;
    a {
      font-size: 100% !important;
      padding: 0 0.8125em;
    }
    span {
      cursor: pointer;
    }
  }
`
