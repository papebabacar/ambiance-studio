import { css } from 'astroturf'

export default css`
  .header {
    grid-column: 1 / span 23;
    grid-row: 1 / span 1;
    display: flex;
    color: #fff;
    background: #2a2a35;
    border-bottom: 2px solid #555;
  }
`
