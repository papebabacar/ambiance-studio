import { css } from 'astroturf'

export default css`
  .menu {
    padding: 0.40625em;
    letter-spacing: 0.08125em;
  }
`
