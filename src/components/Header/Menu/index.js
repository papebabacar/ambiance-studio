import * as Surplus from 'surplus'

import style from './style'

const Component = () => <p class={style.menu}>File Edit New View Settings</p>

export default Component
