import { css } from 'astroturf'

export default css`
  .nav {
    width: 12vw;
    min-width: 100%;
    grid-column: 20 / span 4;
    grid-row: 2 / 16;
    background: rgba(56, 61, 79, 0.1);
    border: 3px solid #555;
  }
`
