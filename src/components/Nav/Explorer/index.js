import * as Surplus from 'surplus'

import style from './style'
import { ui } from '@components/Common'

const Component = () => <div class={`${ui.scrollbar} ${style.explorer}`} />

export default Component
