import { css } from 'astroturf'

export default css`
  .explorer {
    margin-right: 0.5em;
    overflow-y: scroll;
    height: 100%;
    padding: 0.8126em 0.203125em;
    background: rgba(66, 70, 84, 0.51);
    text-align: center;
    div {
      cursor: pointer;
      margin: 0.40625em 0.0625em;
      padding: 0.40625em 0;
      background: linear-gradient(125deg, #5c5c5cac, #4c4c4cec, #0c0c0c8c);
      border: 1px solid #555;
    }
  }
`
