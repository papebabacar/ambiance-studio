import * as Surplus from 'surplus'
import waveforms, { waveformSampleCount, baseFrequency } from './waveforms'
import WavetableWorklet from './wavetable.worklet'
import wsm from './wavetable.wasm'
import AudioKeys from 'audiokeys'
import Worker from './wavetable.worker'
import { Scheduler } from '@core/scheduler'
// console.log(Worker())
let count = 0

let globalGain
let workletHandle
let oscillator
const buildOsc = () => {
  const context = Scheduler.context
  globalGain = new GainNode(context)
  globalGain.gain.value = 0.5
  globalGain.connect(context.destination)

  oscillator = new OscillatorNode(context)
  oscillator.frequency.value = 1
  // oscillator.start()

  // Map the oscillator's output range from [-1, 1] to [0, 1]
  const oscillatorCSN = new ConstantSourceNode(context)
  oscillatorCSN.offset.value = 1 // Add one to the output signals, making the range [0, 2]
  const oscillatorGain = new GainNode(context)
  oscillatorGain.gain.value = 0.5 // Divide the result by 2, making the range [0, 1]

  oscillator.connect(oscillatorCSN.offset)
  oscillatorCSN.connect(oscillatorGain)
  // oscillatorCSN.start()
  const param = workletHandle.parameters.get('dimension_0x1_mix')

  oscillatorGain.connect(param)
  // `gainNode` now outputs a signal in the proper range to modulate our mix param

  const worker = Worker()

  worker.addEventListener('message', e => console.log(e.data))

  worker.postMessage('hello')

  return { globalGain, oscillator, oscillatorGain }
}

const initWavetable = async () => {
  // Register our custom `AudioWorkletProcessor`, and create an `AudioWorkletNode` that serves as a
  // handle to an instance of one.
  const context = Scheduler.context
  await context.audioWorklet.addModule(WavetableWorklet)
  workletHandle = new AudioWorkletNode(context, 'wavetable-node-processor')
  const osc = buildOsc()
  Scheduler.setInstrument(workletHandle, globalGain)
  if (count < 1) workletHandle.connect(globalGain)
  count++
  // workletHandle.parameters.get('frequency')!.value = 516.41;

  // Using those waveforms we generated earlier, construct a flat array of waveform samples with
  // which to fill the wavetable
  const wavetableDef = [
    [waveforms[0], waveforms[1]],
    [waveforms[2], waveforms[3]]
  ]

  const dimensionCount = 2
  const waveformsPerDimension = 2
  const samplesPerDimension = waveformSampleCount * waveformsPerDimension

  const tableSamples = new Float32Array(dimensionCount * waveformsPerDimension * waveformSampleCount)
  for (let dimensionIx = 0; dimensionIx < dimensionCount; dimensionIx++) {
    for (let waveformIx = 0; waveformIx < waveformsPerDimension; waveformIx++) {
      for (let sampleIx = 0; sampleIx < waveformSampleCount; sampleIx++) {
        tableSamples[samplesPerDimension * dimensionIx + waveformSampleCount * waveformIx + sampleIx] =
          wavetableDef[dimensionIx][waveformIx][sampleIx]
      }
    }
  }

  // Send the Wasm module, waveform data, and wavetable settings over to the processor thread
  workletHandle.port.postMessage({
    arrayBuffer: wsm,
    waveformsPerDimension,
    dimensionCount,
    waveformLength: waveformSampleCount,
    baseFrequency,
    tableSamples
  })

  return { ...osc, workletHandle }
}

// const handleSettingsChange = ({ workletHandle, globalGain, oscillatorGain, oscillator }) => {
//   switch (key) {
//     case 'volume': {
//       globalGain.gain.value = val / 150
//       return
//     }
//     case 'frequency': {
//       workletHandle.parameters.get('frequency').value = val
//       return
//     }
//     case 'dim 0 mix': {
//       workletHandle.parameters.get('dimension_0_mix').value = val
//       return
//     }
//     case 'dim 1 mix': {
//       workletHandle.parameters.get('dimension_1_mix').value = val
//       return
//     }
//     case 'dim 0x1 mix': {
//       if (!state['connect oscillator']) {
//         workletHandle.parameters.get('dimension_0x1_mix').value = val
//       }
//       return
//     }
//     case 'connect oscillator': {
//       const param = workletHandle.parameters.get('dimension_0x1_mix')

//       if (val) {
//         param.value = 0
//         oscillatorGain.connect(param)
//       } else {
//         oscillatorGain.disconnect(param)
//         param.value = state['dim 0x1 mix']
//       }
//       return
//     }
//     case 'oscillator frequency': {
//       oscillator.frequency.value = val
//       return
//     }
//     case 'oscillator waveform': {
//       oscillator.type = val
//       return
//     }
//     default: {
//       throw new Error(`Unhandled key: "${key}"`)
//     }
//   }
// }

// const getSettings = toggleStarted => [
//   {
//     type: 'range',
//     label: 'volume',
//     min: 0,
//     max: 100,
//     initial: 5,
//     steps: 100
//   },
//   {
//     type: 'range',
//     label: 'frequency',
//     min: 10,
//     max: 10000,
//     initial: 516.41,
//     scale: 'log',
//     steps: 1000
//   },
//   {
//     type: 'range',
//     label: 'dim 0 mix',
//     min: 0,
//     max: 1,
//     initial: 0,
//     steps: 100
//   },
//   {
//     type: 'range',
//     label: 'dim 1 mix',
//     min: 0,
//     max: 1,
//     initial: 0,
//     steps: 100
//   },
//   {
//     type: 'range',
//     label: 'dim 0x1 mix',
//     min: 0,
//     max: 1,
//     initial: 0,
//     steps: 100
//   },
//   {
//     type: 'checkbox',
//     label: 'connect oscillator',
//     initial: false
//   },
//   {
//     type: 'range',
//     label: 'oscillator frequency',
//     min: 0.01,
//     max: 10000,
//     initial: 2,
//     scale: 'log',
//     steps: 1000
//   },
//   {
//     type: 'select',
//     label: 'oscillator waveform',
//     options: ['sine', 'triangle', 'sawtooth', 'square'],
//     initial: 'sine'
//   },
//   {
//     type: 'button',
//     label: 'start/stop',
//     action: toggleStarted
//   }
// ]

const Wavetable = () => (
  <div
    className="wavetable-demo"
    fn={async () => {
      const params = await initWavetable()
      console.log(params)
    }}
  >
    <input
      type="number"
      oninput={e => {
        workletHandle.parameters.get('frequency').value = Number(e.target.value)
      }}
      min="100"
      value={workletHandle && workletHandle.parameters.get('frequency').value}
      max="1000"
    />
    <input
      type="number"
      oninput={e => {
        workletHandle.parameters.get('dimension_0_mix').value = Number(e.target.value)
      }}
      min="0"
      value={workletHandle && workletHandle.parameters.get('dimension_0_mix').value}
      step="0.01"
      max="1"
    />
    <input
      type="number"
      oninput={e => {
        workletHandle.parameters.get('dimension_1_mix').value = Number(e.target.value)
      }}
      min="0"
      value={workletHandle && workletHandle.parameters.get('dimension_0_mix').value}
      step="0.01"
      max="1"
    />
    <input
      type="number"
      oninput={e => {
        workletHandle.parameters.get('dimension_0x1_mix').value = Number(e.target.value)
      }}
      min="0"
      value={workletHandle && workletHandle.parameters.get('dimension_0_mix').value}
      step="0.01"
      max="1"
    />
    <input
      type="number"
      oninput={e => {
        oscillator.frequency.value = Number(e.target.value)
      }}
      min="0.01"
      value={oscillator && oscillator.frequency.value}
      step="0.01"
      max="10000"
    />
    <input
      type="number"
      oninput={e => {
        globalGain.gain.value = Number(e.target.value)
      }}
      min="0"
      value={globalGain && globalGain.gain.value}
      step="0.1"
      max="1"
    />
  </div>
)

export default Wavetable

const keyboard = new AudioKeys({
  polyphony: 10,
  rows: 2
})

keyboard.down(({ frequency }) => {
  // const { chroma, oct } = Note.props(Note.fromMidi(Note.freqToMidi(frequency)))
  // const noteNumber = 10 * 12 - (chroma + 1) - 12 * oct
  // dispatch(NOTE_ON, { noteNumber })
  // globalGain.gain.linearRampToValueAtTime(0.4, context.currentTime + 0.001)
  workletHandle.parameters.get('frequency').value = frequency

  // const id = uuid()
  // notes[noteNumber] = id
  // state.recording() && state.playing() && dispatch(NOTE_RECORD_ON, { id, noteNumber })
  // clocks[noteNumber] = setInterval(
  //   () => requestAnimationFrame(() => dispatch(NOTE_RECORD_OFF, { id })),
  //   CLOCK_STEP * 4
  // )
})

keyboard.up(({ frequency }) => {
  // globalGain.gain.linearRampToValueAtTime(0, context.currentTime + 0.1)
  // const { chroma, oct } = Note.props(Note.fromMidi(Note.freqToMidi(frequency)))
  // const noteNumber = 10 * 12 - (chroma + 1) - 12 * oct
  // dispatch(NOTE_OFF, { noteNumber })
  // state.recording() && state.playing() && dispatch(NOTE_RECORD_OFF, { id: notes[noteNumber] })
  // clearInterval(clocks[noteNumber])
  // delete notes[noteNumber]
})
