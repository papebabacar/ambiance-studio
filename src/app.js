import '@assets/css/global.css'

import * as Surplus from 'surplus'

import Footer from './components/Footer'
import Header from './components/Header'
import Main from './components/Main'
import Nav from '@components/Nav'
import S from 's-js'
import style from './style'

import './init'
import '@core/scheduler/init'
import './effects'

const App = () => (
  <div class={style.app}>
    <Header />
    <Nav />
    <Main />
    <Footer />
  </div>
)

// document.addEventListener('contextmenu', event => event.preventDefault())

S.root(() => {
  document.body.appendChild(<App />)
})
