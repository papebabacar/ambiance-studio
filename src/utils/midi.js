import S from 's-js'
import { state } from '@store'
import * as Note from 'tonal-note'
import * as Scale from 'tonal-scale'

export function microsecondsPQN(bpm = 120) {
  const minute = 60000000
  return (minute * S.sample(state.denominator)) / 4 / bpm
}
export function bpm(microsecondsPQN = S.sample(state.microsecondsPQN)) {
  const minute = 60000000
  return (minute / microsecondsPQN) * (S.sample(state.denominator) / 4)
}
export function msPerTick(microsecondsPQN = S.sample(state.microsecondsPQN)) {
  const msPQN = microsecondsPQN / 1000
  return msPQN / S.sample(state.ticksPQN)
}
export function barInSeconds(microsecondsPQN = S.sample(state.microsecondsPQN)) {
  const msPQN = microsecondsPQN / 1000000
  return (S.sample(state.numerator) * msPQN) / (S.sample(state.denominator) / 4)
}
export function pxInSeconds(px) {
  const numerator = S.sample(state.numerator)
  const denominator = S.sample(state.denominator)
  const store = S.sample(state.songMode) ? state.playlist : state.pianoroll
  const { gridX } = store()
  const barX = S.sample(state.songMode) ? gridX : (gridX * numerator * 16) / denominator
  return px * (barInSeconds() / barX)
}

export function secondsInPx(seconds) {
  const numerator = S.sample(state.numerator)
  const denominator = S.sample(state.denominator)
  const store = S.sample(state.songMode) ? state.playlist : state.pianoroll
  const { gridX } = store()
  const barX = S.sample(state.songMode) ? gridX : (gridX * numerator * 16) / denominator
  return seconds / (barInSeconds() / barX)
}

export function getPxInTicks(gridX) {
  const beatPQN = 4 * S.sample(state.ticksPQN)
  const beatInPx = S.sample(state.denominator) * gridX
  return beatPQN / beatInPx / S.sample(state.denominator)
}

export function toPlaylist(x) {
  const factorX =
    S.sample(state.playlist).gridX /
    (((S.sample(state.numerator) * 16) / S.sample(state.denominator)) * S.sample(state.pianoroll).gridX)
  return x * factorX
}

export function getPianorollSnapWidth(snap) {
  const numerator = S.sample(state.numerator)
  const denominator = S.sample(state.denominator)
  const { gridX } = state.pianoroll()
  if (snap === 'BAR') return (gridX * numerator * 16) / denominator
  if (snap === 'BEAT') return (gridX * 16) / denominator
  if (snap === '1/2BEAT') return (gridX * 16) / denominator / 2
  if (snap === '1/3BEAT') return (gridX * 16) / denominator / 3
  if (snap === '1/4BEAT') return (gridX * 16) / denominator / 4
  if (snap === '1/6BEAT') return (gridX * 16) / denominator / 6
  if (snap === '1/12BEAT') return (gridX * 16) / denominator / 12
  if (snap === '1/16BEAT') return (gridX * 16) / denominator / 16
  if (snap === '1/32BEAT') return (gridX * 16) / denominator / 32
  if (snap === '1/64BEAT') return (gridX * 16) / denominator / 64
  if (snap === 'NONE') return 1
}

export function getPlaylistSnapWidth(snap) {
  const numerator = S.sample(state.numerator)
  const { gridX } = state.playlist()
  if (snap === 'BAR') return gridX
  if (snap === 'BEAT') return gridX / numerator
  if (snap === '1/2BEAT') return gridX / (numerator * 2)
  if (snap === '1/3BEAT') return gridX / (numerator * 3)
  if (snap === '1/4BEAT') return gridX / (numerator * 4)
  if (snap === '1/6BEAT') return gridX / (numerator * 6)
  if (snap === '1/12BEAT') return gridX / (numerator * 12)
  if (snap === '1/16BEAT') return gridX / (numerator * 16)
  if (snap === '1/32BEAT') return gridX / (numerator * 32)
  if (snap === 'NONE') return 1
}

export function getFrequency(y) {
  const scale = Scale.notes('C chromatic')
  const octave = 9 - Math.floor(y / (12 * S.sample(state.pianoroll).gridY))
  const note = (9 - octave) * 12 + (11 - y / S.sample(state.pianoroll).gridY)
  return Note.freq(scale[note] + octave)
}
