export const reverseRect = (r1, r2) => {
  let r1x = r1.x,
    r1y = r1.y,
    r2x = r2.x,
    r2y = r2.y,
    d
  if (r1x > r2x) {
    d = Math.abs(r1x - r2x)
    r1x = r2x
    r2x = r1x + d
  }
  if (r1y > r2y) {
    d = Math.abs(r1y - r2y)
    r1y = r2y
    r2y = r1y + d
  }
  return { x1: r1x, y1: r1y, x2: r2x, y2: r2y } // return the corrected rect.
}

export const hitCheck = (s1, s2) => {
  // corners of shape 1
  const X1 = s1.x
  const Y1 = s1.y
  const A1 = s1.x + s1.width
  const B1 = s1.y + s1.height

  // corners of shape 2
  const X2 = s2.x
  const A2 = s2.x + s2.width
  const Y2 = s2.y
  const B2 = s2.y + s2.height

  // Simple overlapping rect collision test
  if (A1 <= X2 || A2 <= X1 || B1 <= Y2 || B2 <= Y1) {
    return false
  } else {
    return true
  }
}

export const hitCheckX = ({ x: x1, width: width1 }, { x: x2, width: width2 }) =>
  hitCheck(
    {
      x: x1,
      y: 0,
      width: width1,
      height: 1
    },
    {
      x: x2,
      y: 0,
      width: width2,
      height: 1
    }
  )

export const roundToNearest = (number, multiple) => {
  const half = multiple / 2
  const closer = number + half - ((number + half) % multiple)
  const larger = closer + multiple
  return number - closer > larger - number ? larger : closer
}

export const clearMouseEvents = () => {
  window.onmousemove = () => null
  window.onmouseup = () => null
}

export const snapLength = (length, bar) =>
  length % bar === 0 ? Math.max(length, bar) : Math.max(length - (length % bar) + bar, bar)

class MixinBuilder {
  constructor(superclass) {
    this.superclass = superclass
  }

  with(...mixins) {
    return mixins.reduce((c, mixin) => mixin(c), this.superclass)
  }
}
export const mix = superclass => new MixinBuilder(superclass)
