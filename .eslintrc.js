const PRE_COMMIT = process.env.PRE_COMMIT

module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
    es6: true,
    jest: true
  },
  extends: ['last', 'prettier/react', 'plugin:react/recommended'],
  rules: {
    'prettier/prettier': ['error', {}],
    'no-unused-vars': 2,
    'no-debugger': PRE_COMMIT ? 'error' : 'off',
    'no-console': PRE_COMMIT ? 'error' : 'off',
    'react/prop-types': 'off',
    'react/no-unknown-property': 'off',
    'react/display-name': 'off'
  },
  settings: {
    react: {
      pragma: 'Surplus'
    }
  }
}
